import pymongo
import json
import os
import requests
from Classes.Tools import Tools

Decode = Tools.decode
Initer = Tools.initer

dirpath = os.getcwd()

DEBUG = False

if DEBUG is not True:
    with open(os.path.join(dirpath, "Database/DatabaseConfigs.json")) as f:
        configs = json.load(f)

    get_database_url = json.loads(
        requests.get("{0}/Services/config/get/ChiChiShop".format(configs['MicroServiceManagementURl']),
                     verify=False).content)

    if not get_database_url["State"]:
        exit(1)

    get_database_url = json.loads(get_database_url["Description"])

    url = ''
    if get_database_url["Key"] is None:
        url = Decode(get_database_url["DatabaseString"])
    else:
        Initer(get_database_url["Key"])
        url = Decode(get_database_url["DatabaseString"])
else:
    url = 'localhost:27017'
    configs = {
        'DatabaseName': 'ChiChiShop',
        'Product': 'Product',
        'Basket': 'Basket',
        'Checkout': 'Checkout',
        'ShopConfig': 'ShopConfig'
    }

mongodb = pymongo.MongoClient(url)
database = mongodb[configs["DatabaseName"]]
product_collection = database[configs["Product"]]
basket_collection = database[configs["Basket"]]
checkout_collection = database[configs['Checkout']]
shop_config_collection = database[configs['ShopConfig']]
category_collection = database[configs["Category"]]
package_collection = database[configs['Package']]
search_history_collection = database[configs["SearchHistory"]]
