from Classes.Basket import Basket
from flask import Blueprint, request
from Middleware.Middlewares import json_body_required, login_required, check_form_json_key
from Classes.Tools import Tools

checkout_route = Blueprint("checkout_route", __name__, "template")


@checkout_route.route('/checkout/submit/userinfo', methods=['POST'])
@login_required
@json_body_required
@check_form_json_key(['BasketId', 'Name', 'PhoneNumber', 'Location', 'Receipt', 'DeliveryType'])
def submit_user_info():
    try:
        data = request.get_json()
        return Basket.submit_user_info(request.headers['Id'],
                                         data['BasketId'],
                                         data['Name'],
                                         data['PhoneNumber'],
                                         data['Location'],
                                         data['Receipt'],
                                         data['DeliveryType'])
    except Exception as ex:
        import traceback
        traceback.print_exc()
        return Tools.Result(False, ex.args)


@checkout_route.route('/checkout/delivery/prices/<latitude>/<longitude>', methods=['GET'])
@login_required
def get_price_for_delivery(latitude, longitude):
    try:
        return Basket.get_price_for_delivery(latitude, longitude)
    except Exception as ex:
        return Tools.Result(False, ex.args)


# @checkout_route.route('/checkout/submit/deliveryinfo', methods=['POST'])
# @login_required
# @json_body_required
# @check_form_json_key(['BasketId', 'DeliveryType'])
# def submit_chosen_delivery():
#     try:
#         data = request.get_json()
#         return Basket.submit_chosen_delivery(request.headers['Id'],
#                                                data['BasketId'],
#                                                data['DeliveryType'])
#     except Exception as ex:
#         import traceback
#         traceback.print_exc()
#         return Tools.Result(False, ex.args)


@checkout_route.route('/checkout/submit/paymentinfo', methods=['POST'])
@login_required
@json_body_required
@check_form_json_key(['BasketId', 'PaymentType'])
def submit_payment_info():
    try:
        data = request.get_json()
        return Basket.submit_payment_info(request.headers['Id'],
                                            data['BasketId'],
                                            data['PaymentType'])
    except Exception as ex:
        return Tools.Result(False, ex.args)


@checkout_route.route('/checkout/packageinfo/<user_id>/<basket_id>', methods=['GET'])
@login_required
def get_package_info(user_id, basket_id):
    try:
        return Basket.get_package_info(user_id, basket_id)
    except Exception as ex:
        import traceback
        traceback.print_exc()
        return Tools.Result(False, ex.args)


@checkout_route.route('/admin/checkout/confirm/received', methods=['POST'])
@json_body_required
@check_form_json_key(['BasketId'])
def confirm_package_received():
    try:
        data = request.get_json()
        return Basket.confirm_package_received(request.headers['Id'], data['BasketId'])
    except Exception as ex:
        return Tools.Result(False, ex.args)


@checkout_route.route('/admin/checkout/confirm/gathering', methods=['POST'])
@json_body_required
@check_form_json_key(['BasketId'])
def confirm_package_gathering():
    try:
        data = request.get_json()
        return Basket.confirm_package_gathering(request.headers['Id'], data['BasketId'])
    except Exception as ex:
        import traceback
        traceback.print_exc()
        return Tools.Result(False, ex.args)
