from Authentication.Authentication import is_login, permission
from fastapi import APIRouter, Form, Depends
from fastapi import HTTPException
from starlette.requests import Request
from Classes.Basket import Basket
from Model.Products import AdminAddProductModel, AdminUpdateProductModel, ProductDetailPage
from Classes.Tools import Tools
from Classes.Product import Product
from datetime import datetime

product_route = APIRouter()


@product_route.post('/admin/product/add', dependencies=[Depends(is_login)], tags=["product"])
def add_product(
        data: AdminAddProductModel

):
    try:
        return Product.add_product(data.UniqueValue,
                                   data.Name,
                                   data.Attribute,
                                   data.Manufacture,
                                   data.Count,
                                   data.Price,
                                   datetime.now(),
                                   datetime.now(),
                                   data.Description,
                                   data.Category,
                                   data.Images,
                                   off_percentage=data.Off,
                                   is_off_enabled=data.IsOffEnable,
                                   sub_category=data.SubCategory
                                   )
    except HTTPException as ex:
        return Tools.Result(False, ex.args)


# , dependencies=[Depends(is_login)]
@product_route.post('/admin/product/update', tags=["product"])
def update_product(
        data: AdminUpdateProductModel
):
    try:
        return Product.update_product(unique_value=data.UniqueValue, name=data.Name, attribute=data.Attribute,
                                      count=data.Count,
                                      prev_price=data.Price, description=data.Description, category=data.Category,
                                      images=data.Images, is_off_enabled=data.IsOffEnable,
                                      sub_category=data.SubCategory, manufacture=data.Manufacture,
                                      off_percentage=data.Off, _id=data.Id)
    except HTTPException as ex:
        return Tools.Result(False, ex.args)


@product_route.post('/admin/product/delete', dependencies=[Depends(is_login)], tags=["product"])
def delete_product(UniqueValue: str = Form(...)):
    try:
        return Product.delete_product(UniqueValue)
    except Exception as ex:
        return Tools.Result(False, ex.args)


# dependencies=[Depends(is_login)],
@product_route.get('/admin/product/all', tags=["product"])
# chashing
def get_all_products(page: int = 1):
    # try:
    return Product.get_products(page)


# except Exception as ex:
#     return Tools.Result(False, ex.args)

# dependencies=[Depends(is_login)],
# todo : add login and permission
@product_route.get('/admin/product/{_id}', tags=["product"])
def get_product_info(_id):
    try:
        return Product.get_product(_id)
    except HTTPException as ex:
        return Tools.Result(False, ex.args)


@product_route.get('/admin/products/get/{id}', description="admin get product with id",
                   tags=['Product'])
def admin_get_product_by_id(id):
    return Product.admin_get_product(_id=id)


# dependencies=[Depends(is_login)],
@product_route.get('/admin/product/in/{sub_category}', tags=["product"])
# caching
def get_product_by_category(sub_category, page: int = 1):
    try:
        return Product.get_product_by_category(sub_category, page=page)
    except HTTPException as ex:
        return Tools.Result(False, ex.args)


@product_route.get('/mobile/product/detail-page/{id}', tags=["product"], response_model=ProductDetailPage)
def mobile_product_detail_page(id):
    return Product.mobile_product_page(id)


@product_route.get('/system/homepage/query/{item}/{key}', tags=["product"])
# todo : system reque
def system_homepage_query(item, key):
    return Product.system_homepage_query(item, key)


@product_route.get("/mobile/product/sub-category-page", tags=["Category"], description="subcategory page")
def mobile_sub_category_page(category: str = None, id: str = None, page: int = 1):
    if not category and not id:
        raise HTTPException(status_code=400, detail="category name and id are None")
    if category and id:
        raise HTTPException(status_code=400, detail="category name and id cant assigning together")
    return Product.load_products_of_sub_category(category, id, page)


@product_route.get('/mobile/load-more', tags=["Load-More"],
                   description='load more[sub-category,item-list,product-recommendation,search-recommendation]',
                   dependencies=[Depends(is_login)])
def mobile_load_more(request: Request, key, value, page: int = 1):
    return Product.load_more(request.headers["Id"], key, value, page)


@product_route.get('/test/test', tags=["product"])
def test():
    return Product.test()
