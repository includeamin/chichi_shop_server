from Authentication.Authentication import is_login, permission
from fastapi import APIRouter, Form, Depends
from fastapi import HTTPException
from starlette.requests import Request
from Classes.Tools import Tools
from Classes.Product import Product
from datetime import datetime
from Model.CategoryModel import AddCategoryModel
from Classes.Category import Category

category_route = APIRouter()


@category_route.post('/admin/category/add', description='admin add category', tags=['Category'])
def admin_add(data: AddCategoryModel):
    return Category.admin_add(data.name, data.image)


@category_route.post("/admin/category/sub-category/{action}", description='action = add , delete', tags=["Category"])
def admin_sub_category_action(action, category, sub_category):
    valid_actions = ['add', 'delete']
    if action not in valid_actions:
        raise HTTPException(detail=f'action can {valid_actions}', status_code=400)
    if action == 'add':
        return Category.admin_add_sub_category(category, sub_category)
    elif action == 'delete':
        return Category.admin_remove_sub_category(category, sub_category)


@category_route.get("/category/image/update", tags=['Category'])
def update_category_image(Id, new_image):
    return Category.admin_update_image(Id, new_image)


@category_route.get('/admin/category/get-all', description='admin get all category', tags=['Category'])
def admin_get_all():
    return Category.admin_get_all()


@category_route.delete('/admin/category/delete/{name}', description='admin delete category by name', tags=['Category'])
def admin_delete_by_name(name):
    return Category.delete(name)


@category_route.get('/admin/category/get/detail', description='admin get detail of category key=[name,id]',
                    tags=['Category'])
def admin_get_category_detail(key, value):
    return Category.admin_get_by(key, value)


@category_route.get('/category/get/all-name',
                    description='get categories name and id, suitable for drop-down,item-list or  ...',
                    tags=['Category'])
def category_get_all_name():
    return Category.get_all_category_name()


@category_route.get("/admin/category/get/list-image-id", tags=["HomePage", 'Category'])
def admin_category_list_image_id():
    return Category.homepage_get_category_images_list()
