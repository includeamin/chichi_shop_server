from Authentication.Authentication import is_login, permission
from fastapi import APIRouter, Form, Depends
from fastapi import HTTPException
from Classes.Tools import Tools
from Classes.session import SessionActions

session_route = APIRouter()


@session_route.get("/system/shop/start_session", tags=["SystemSession"])
def start_session(user_id):
    return SessionActions.system_start(user_id=user_id)
