from Classes.ShopConfig import ShopConfig
from flask import Blueprint, request
from Middleware.Middlewares import json_body_required, login_required, check_form_json_key
from Classes.Tools import Tools

shop_route = Blueprint("shop_route", __name__, "template")


@shop_route.route('/admin/config/init', methods=['POST'])
@json_body_required
@login_required
def init_shop_config():
    try:
        return ShopConfig.init_config()
    except Exception as ex:
        return Tools.Result(False, ex.args)

@shop_route.route('/admin/set/distancecost', methods=['POST'])
@json_body_required
@check_form_json_key(['Distances'])
def set_distance_cost():
    try:
        data = request.get_json()
        return ShopConfig.set_distance_cost(data['Distances'])
    except Exception as ex:
        return Tools.Result(False, ex.args)


@shop_route.route('/admin/set/message/gathering', methods=['POST'])
@json_body_required
@check_form_json_key(['Message'])
def set_product_gathering_message():
    try:
        data = request.get_json()
        return ShopConfig.set_product_gathering_message(data['Message'])
    except Exception as ex:
        return Tools.Result(False, ex.args)


@shop_route.route('/admin/set/message/received', methods=['POST'])
@json_body_required
@check_form_json_key(['Message'])
def set_operator_received_package_message():
    try:
        data = request.get_json()
        return ShopConfig.set_operator_received_package_message(data['Message'])
    except Exception as ex:
        return Tools.Result(False, ex.args)


@shop_route.route('/admin/set/shopgeo', methods=['POST'])
@json_body_required
@check_form_json_key(['Geo'])
def set_shop_center_geo():
    try:
        data = request.get_json()
        return ShopConfig.set_shop_center_geo(data['Geo'])
    except Exception as ex:
        return Tools.Result(False, ex.args)


@shop_route.route('/admin/set/uniquevalue', methods=['POST'])
@json_body_required
@check_form_json_key(['UniqueValue'])
def set_unique_value():
    try:
        data = request.get_json()
        return ShopConfig.set_unique_value(data['UniqueValue'])
    except Exception as ex:
        return Tools.Result(False, ex.args)

@shop_route.route('/shopgeo', methods=['GET'])
def get_shop_center_geo():
    try:
        return ShopConfig.get_shop_center_geo()
    except Exception as ex:
        return Tools.Result(False, ex.args)
