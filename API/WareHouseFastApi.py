from Authentication.Authentication import is_login, permission
from fastapi import APIRouter, Form, Depends
from fastapi import HTTPException
from starlette.requests import Request
from Classes.WareHouseManagement import WareHouse

ware_house = APIRouter()


@ware_house.get("/warehouse/baskets/get/state", tags=["WareHouse"], description='get basket by states')
def ware_house_get_baskets_by_state(state):
    return WareHouse.ware_house_get_basket_depends_of_state(state)


@ware_house.get('/warehouse/states', tags=["WareHouse"], description='get warehouse valid states')
def ware_house_get_valid_states():
    return WareHouse.get_valid_ware_house_state()


@ware_house.get('/warehouse/baskets/state/action', tags=["WareHouse"], description='change baskets state')
def ware_house_basket_state_change(basket_id, action="Gathering"):
    if action == "Gathering":
        return WareHouse.confirm_package_received(basket_id)
    elif action == "WaitForAssign":
        return WareHouse.confirm_package_gathering(basket_id)
    else:
        raise HTTPException(detail='wrong action, action can be Gathering or WaitForAssign', status_code=400)


@ware_house.get("/warehouse/baskets/detail", tags=["WareHouse"])
def warehouse_baskets_get_detail(_id):
    return WareHouse.ware_house_get_package_info(_id)


@ware_house.get("/warehouse/baskets/valid-states",tags=["WareHouse"])
def warehouse_valid_states_of_package():
    return WareHouse.get_valid_ware_house_state()
