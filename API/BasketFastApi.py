from Authentication.Authentication import is_login, permission
from fastapi import APIRouter, Form, Depends
from fastapi import HTTPException
from starlette.requests import Request
from Classes.Basket import Basket
from Classes.Tools import Tools
from Model.BasketModel import BasketAddModel, DeleteItemFromBasketModel, DeleteAllModel, DeleteAllFromBasket
from bson import ObjectId

basket_route = APIRouter()


# @basket_route.post("/basket/add", description="Get Basket", tags=["Basket"], dependencies=[Depends(is_login)])
# def add_to_basket(request: Request, BasketId: str = Form(...),
#                   ProductId: str = Form(...),
#                   Number: str = Form(...)
#                   ):
@basket_route.post("/basket/add", description="Get Basket", tags=["Basket"], dependencies=[Depends(is_login)])
def add_to_basket(request: Request, data: BasketAddModel):
    # try:
    # basket_id = data['BasketId'] if 'BasketId' in data else None
    if data.BasketId is not None:
        if not ObjectId.is_valid(data.BasketId):
            raise HTTPException(detail="Invalid BasketId format", status_code=400)

    return Basket.add_to_basket(request.headers['Id'],
                                data.ProductId,
                                data.Number

                                )


# except HTTPException as ex:
#     import traceback
#     traceback.print_exc()
#     return Tools.Result(False, ex.args)


@basket_route.get('/basket/get/id', tags=["Basket"], dependencies=[Depends(is_login)])
def get_basket_id(request: Request):
    try:
        return Basket.get_basket_id(request.headers['Id'])
    except Exception as ex:
        return Tools.Result(False, ex.args)


@basket_route.post('/basket/delete', description="delete animation", tags=["Basket"], dependencies=[Depends(is_login)])
def delete_from_basket(request: Request, data: DeleteItemFromBasketModel):
    try:
        return Basket.delete_from_basket(request.headers['Id'],
                                         data.ProductId)
    except Exception as ex:
        return Tools.Result(False, ex.args)


@basket_route.post('/basket/delete/all', description="Delete all basket animation", tags=['Basket'],
                   dependencies=[Depends(is_login)])
def delete_all_instances(request: Request, data: DeleteAllFromBasket):
    try:
        return Basket.delete_all_instances(request.headers['Id'],
                                           data.ProductId)
    except Exception as ex:
        return Tools.Result(False, ex.args)


#

@basket_route.get('/basket', dependencies=[Depends(is_login)], description="Get all item of basket", tags=["Basket"])
def get_basket_items(request: Request):
    try:
        return Basket.get_basket_items(request.headers['Id'])
    except Exception as ex:
        return Tools.Result(False, ex.args)


@basket_route.get('/basket/filter/get', dependencies=[Depends(is_login)], description="Filter Basket", tags=["Basket"])
def get_all_baskets(request: Request, state=None):
    try:

        state = None if state == 'all' else state
        return Basket.get_all_baskets(request.headers['Id'], state)
    except Exception as ex:
        return Tools.Result(False, ex.args)
