from Classes.Product import Product
from flask import Blueprint, request
from Middleware.Middlewares import json_body_required, login_required, check_form_json_key
from Classes.Tools import Tools
import traceback

product_route = Blueprint("product_route", __name__, "template")


@product_route.route('/admin/product/add', methods=['POST'])
@login_required
@json_body_required
@check_form_json_key(
    ['UniqueValue', 'Name', 'Attribute', 'Manufacture', 'Count', 'Created_at', 'Updated_at', 'price',
     'CurrentPrice', 'Description', 'Category', 'Images'])
def add_product():
    try:
        data = request.get_json()
        return Product.add_product(data['UniqueValue'],
                                   data['Name'],
                                   data['Attribute'],
                                   data['Manufacture'],
                                   data['Count'],
                                   data['price'],
                                   data['Created_at'],
                                   data['Updated_at'],
                                   data['CurrentPrice'],
                                   data['Description'],
                                   data['Category'],
                                   data['Images'])
    except Exception as ex:
        traceback.print_exc()
        return Tools.Result(False, ex.args)


@product_route.route('/admin/product/update', methods=['POST'])
@login_required
@json_body_required
@check_form_json_key(
    ['UniqueValue', 'Name', 'Attribute', 'Manufacture', 'Count', 'price', 'CurrentPrice', 'Description', 'Category',
     'Images'])
def update_product():
    try:
        data = request.get_json()
        return Product.update_product(data['UniqueValue'],
                                      data['Name'],
                                      data['Attribute'],
                                      data['Manufacture'],
                                      data['Count'],
                                      data['price'],
                                      data['CurrentPrice'],
                                      data['Description'],
                                      data['Category'],
                                      data['Images'])
    except Exception as ex:
        return Tools.Result(False, ex.args)


@product_route.route('/admin/product/delete', methods=['POST'])
@login_required
@json_body_required
@check_form_json_key(['UniqueValue'])
def delete_product():
    try:
        data = request.get_json()
        return Product.delete_product(data['UniqueValue'])
    except Exception as ex:
        return Tools.Result(False, ex.args)


@product_route.route('/admin/product/all', methods=['GET'])
@login_required
def get_all_products():
    try:
        return Product.get_products()
    except Exception as ex:
        return Tools.Result(False, ex.args)


@product_route.route('/admin/product/<unique_value>')
@login_required
def get_product_info(unique_value):
    try:
        return Product.get_product(unique_value)
    except Exception as ex:
        return Tools.Result(False, ex.args)


@product_route.route('/admin/product/in/<category>')
@login_required
def get_product_by_category(category):
    try:
        return Product.get_product_by_category(category)
    except Exception as ex:
        return Tools.Result(False, ex.args)


@product_route.route('/system/homepage/query/<item>/<key>')
# todo : system reque
def system_homepage_query(item, key):
    return Product.system_homepage_query(item, key)


@product_route.route('/test/test')
def test():
    return Product.test()
