from Classes.Basket import Basket
from flask import Blueprint, request
from Middleware.Middlewares import json_body_required, login_required, check_form_json_key
from Classes.Tools import Tools
from bson import ObjectId

basket_route = Blueprint("basket_route", __name__, "template")


@basket_route.route('/basket/add', methods=['POST'])
@json_body_required
@login_required
@check_form_json_key(['ProductId', 'Number'])
def add_to_basket():
    try:
        data = request.get_json()

        basket_id = data['BasketId'] if 'BasketId' in data else None
        if basket_id is not None:
            if ObjectId.is_valid(basket_id):
                raise Exception("Invalid BasketId format")

        return Basket.add_to_basket(request.headers['Id'],
                                    data['ProductId'],
                                    data['Number'],
                                    basket_id
                                    )
    except Exception as ex:
        import traceback
        traceback.print_exc()
        return Tools.Result(False, ex.args)


@basket_route.route('/basket/id', methods=['GET'])
@login_required
def get_basket_id():
    try:
        return Basket.get_basket_id(request.headers['Id'])
    except Exception as ex:
        return Tools.Result(False, ex.args)


@basket_route.route('/basket/delete', methods=['POST'])
@login_required
@json_body_required
@check_form_json_key(['ProductId', 'BasketId'])
def delete_from_basket():
    try:
        data = request.get_json()
        return Basket.delete_from_basket(request.headers['Id'],
                                         data['BasketId'],
                                         data['ProductId'])
    except Exception as ex:
        return Tools.Result(False, ex.args)


@basket_route.route('/basket/delete/all', methods=['POST'])
@login_required
@json_body_required
@check_form_json_key(['ProductId', 'BasketId'])
def delete_all_instances():
    try:
        data = request.get_json()
        return Basket.delete_all_instances(request.headers['Id'],
                                           data['BasketId'],
                                           data['ProductId'])
    except Exception as ex:
        return Tools.Result(False, ex.args)


@basket_route.route('/basket/<basket_id>', methods=['GET'])
@login_required
def get_basket_items(basket_id):
    try:
        return Basket.get_basket_items(request.headers['Id'], basket_id)
    except Exception as ex:
        return Tools.Result(False, ex.args)


@basket_route.route('/basket/filter/<state>', methods=['GET'])
@login_required
def get_all_baskets(state):
    try:
        state = None if state == 'all' else state
        return Basket.get_all_baskets(request.headers['Id'], state)
    except Exception as ex:
        return Tools.Result(False, ex.args)
# @basket_route.route('/basket/confirm', methods=['POST'])
# @login_required
# @json_body_required
# @check_form_json_key(['UserId'])
# def confirm_basket():
#     try:
#         data = request.get_json()
#         return Basket.confirm_basket(data['UserId'])
#     except Exception as ex:
#         return Tools.Result(False, ex.args)


# @basket_route.route('/basket/undoconfirm', methods=['POST'])
# @login_required
# @json_body_required
# @check_form_json_key(['UserId'])
# def undo_confirm_basket():
#     try:
#         data = request.get_json()
#         return Basket.undo_confirmation(data['UserId'])
#     except Exception as ex:
#         return Tools.Result(False, ex.args)
