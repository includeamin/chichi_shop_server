from Classes.ShopConfig import ShopConfig
from Classes.Tools import Tools
from fastapi import APIRouter, Form
from Authentication.Authentication import is_login
from starlette.requests import Request
from fastapi import HTTPException


shop_route = APIRouter()


@shop_route.post('/admin/config/init',tags=["ShopConfigs"])
def init_shop_config():
    try:
        return ShopConfig.init_config()
    except HTTPException as ex:
        return Tools.Result(False, ex.args)


@shop_route.post('/admin/set/distancecost',tags=["ShopConfigs"])
def set_distance_cost(Min: int = Form(...), Max: int = Form(...)):
    try:
        return ShopConfig.set_distance_cost({"Min": Min, "Max": Max})
    except HTTPException as ex:
        return Tools.Result(False, ex.args)


@shop_route.post('/admin/set/message/gathering',tags=["ShopConfigs"])
def set_product_gathering_message(Message: str = Form(...)):
    try:
        return ShopConfig.set_product_gathering_message(Message)
    except HTTPException as ex:
        return Tools.Result(False, ex.args)


@shop_route.post('/admin/set/message/received',tags=["ShopConfigs"])
def set_operator_received_package_message(Message: str = Form(...)):
    try:
        return ShopConfig.set_operator_received_package_message(Message)
    except HTTPException as ex:
        return Tools.Result(False, ex.args)


@shop_route.post('/admin/set/shopgeo',tags=["ShopConfigs"])
def set_shop_center_geo(Lang: int = Form(...), Lat: int = Form(...)):
    try:
        return ShopConfig.set_shop_center_geo({"Lang": Lang,
                                               "Lat": Lat})
    except HTTPException as ex:
        return Tools.Result(False, ex.args)


@shop_route.post('/admin/set/uniquevalue',tags=["ShopConfigs"])
def set_unique_value(UniqueValue:str=Form(...)):
    try:
        return ShopConfig.set_unique_value(UniqueValue)
    except HTTPException as ex:
        return Tools.Result(False, ex.args)


@shop_route.get('/shopgeo',tags=["ShopConfigs"])
def get_shop_center_geo():
    try:
        return ShopConfig.get_shop_center_geo()
    except HTTPException as ex:
        return Tools.Result(False, ex.args)
