from Classes.Search import Search

from Authentication.Authentication import is_login, permission
from fastapi import APIRouter, Form, Depends
from fastapi import HTTPException
from starlette.requests import Request
from Classes.Tools import Tools

from Model.Search import SearchResponseModel, SearchPageRecommendationModel

search_route = APIRouter()


@search_route.get("/mobile/product/search", description="search", tags=["Search"], response_model=SearchResponseModel)
def search(key: str, page: int = 1):
    return Search.search(key, page)


@search_route.get('/admin/product/search', description="search", tags=["Admin", "Search"])
def admin_search(key: str, page: int = 1):
    return Search.admin_search_products(key, page)


@search_route.get('/mobile/search/add-to-history', description='add item to user history', tags=['Search'],
                  dependencies=[Depends(is_login)])
def add_product_to_user_history(request: Request, product_name, category, product_id, sub_category):
    return Search.add_search_history(user_id=request.headers["Id"], product_name=product_name, category=category,
                                     product_id=product_id, sub_category=sub_category)


@search_route.get('/mobile/search/recommendation', description='get Recommendation depends on user search',
                  tags=["Search"], dependencies=[Depends(is_login)], response_model=SearchPageRecommendationModel)
def get_user_recommendation_depends_on_search(request: Request, page: int = 1):
    return Search.recommendation_depend_of_history(user_id=request.headers["Id"], page=page)


@search_route.get("/admin/search/homepage/destination", tags=["HomePage", "Search"],
                  description="search for destinations of homepages hiderslider, destination name can be [Product,Category]")
def admin_search_destination_of_homepage(destination_name, key):
    return Search.admin_homepage_destination_search(destination_name, key)
