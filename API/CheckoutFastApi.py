from Authentication.Authentication import is_login, permission
from fastapi import APIRouter, Form, Depends
from fastapi import HTTPException
from starlette.requests import Request
from Classes.Basket import Basket
from Classes.Tools import Tools
from Model.CheckoutModels import CheckoutSubmitUserInfo, SubmitPaymentInfo

checkout_route = APIRouter()


# @checkout_route.post('/checkout/submit/userinfo', dependencies=[Depends(is_login)],tags=["Checkout"])
# def submit_user_info(request: Request,
#                      BasketId: str = Form(...),
#                      Name: str = Form(...),
#                      PhoneNumber: str = Form(...),
#                      Location: str = Form(...),
#                      Receipt: str = Form(...),
#                      DeliveryType: str = Form(...)
#                      ):
#     try:
#         return Basket.submit_user_info(request.headers['Id'],
#                                        BasketId,
#                                        Name,
#                                        PhoneNumber,
#                                        Location,
#                                        Receipt,
#                                        DeliveryType)
#     except HTTPException as ex:
#         import traceback
#         traceback.print_exc()
#         return Tools.Result(False, ex.args)


@checkout_route.post('/checkout/submit/userinfo', dependencies=[Depends(is_login)], tags=["Checkout"])
def submit_user_info(request: Request,
                     data: CheckoutSubmitUserInfo
                     ):
    try:
        return Basket.submit_user_info(request.headers['Id'],
                                       data.Name,
                                       data.PhoneNumber,
                                       data.Location,
                                       data.Receipt,
                                       data.DeliveryType)
    except HTTPException as ex:
        import traceback
        traceback.print_exc()
        return Tools.Result(False, ex.args)


@checkout_route.get('/checkout/delivery/prices/{latitude}/{longitude}', dependencies=[Depends(is_login)],
                    tags=["Checkout"])
def get_price_for_delivery(latitude, longitude):
    try:
        return Basket.get_price_for_delivery(latitude, longitude)
    except HTTPException as ex:
        return Tools.Result(False, ex.args)


# @checkout_route.post('/checkout/submit/paymentinfo', dependencies=[Depends(is_login)],tags=["Checkout"])
# def submit_payment_info(request: Request, BasketId: str = Form(...),
#                         PaymentType: str = Form(...)
#                         ):
#     try:
#         return Basket.submit_payment_info(request.headers['Id'],
#                                           BasketId,
#                                           PaymentType)
#     except HTTPException as ex:
#         return Tools.Result(False, ex.args)
@checkout_route.post('/checkout/submit/paymentinfo', dependencies=[Depends(is_login)], tags=["Checkout"])
def submit_payment_info(request: Request, data: SubmitPaymentInfo
                        ):
    try:
        return Basket.submit_payment_info(request.headers['Id'],

                                          data.PaymentType)
    except HTTPException as ex:
        return Tools.Result(False, ex.args)


@checkout_route.get("/system/checkout/finalize_payment", tags=["System"])
def system_finalize_payment_info(user_id):
    return Basket.finalize_payment(user_id=user_id)


# dependencies=[Depends(is_login)],

@checkout_route.get('/checkout/packageinfo/{package_code}', dependencies=[Depends(is_login)],
                    tags=["Checkout", "WareHouse"])
def get_package_info(package_code: int):
    return Basket.get_basket_info(package_code)


@checkout_route.get("/checkout/package-summery-of-payment",dependencies=[Depends(is_login)], tags=["Checkout"])
def mobile_get_summery_of_payment(package_code: int):
    return Basket.get_summery_of_payment(package_code)

# @checkout_route.post('/admin/checkout/confirm/received', dependencies=[Depends(is_login)], tags=["Checkout"])
# def confirm_package_received(request: Request, BasketId: str = Form(...)):
#     try:
#         return Basket.confirm_package_received(request.headers['Id'], BasketId)
#     except HTTPException as ex:
#         return Tools.Result(False, ex.args)


# @checkout_route.post('/admin/checkout/confirm/gathering', dependencies=[Depends(is_login)], tags=["Checkout"])
# def confirm_package_gathering(request: Request, BasketId: str = Form(...)):
#     try:
#         return Basket.confirm_package_gathering(request.headers['Id'], BasketId)
#     except HTTPException as ex:
#         import traceback
#         traceback.print_exc()
#         return Tools.Result(False, ex.args)
