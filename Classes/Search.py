import pymongo

from Classes.Tools import Tools
from Database.DB import product_collection, search_history_collection, category_collection
from Model.Search import SearchResponseModel, SearchHistoryModel, SearchPageRecommendationModel

from fastapi import HTTPException


class Search:
    @staticmethod
    def search(key: str, page=1):
        skip, page = Tools.pagination(page)
        category_list = []
        key = key.split('_')[1]
        data = product_collection.find({"$or": [{"Category": {'$regex': f'^{key}'}},
                                                {"SubCategory": {'$regex': f'^{key}'}},
                                                {"Name": {'$regex': f'^{key}'}},
                                                {"UniqueValue": {'$regex': f'^{key}'}},
                                                {"Manufacture": {'$regex': f'^{key}'}}]}, {
                                           "Images",
                                           "Category",
                                           "SubCategory",
                                           "UniqueValue",
                                           "Name",
                                           "Manufacture",
                                           "Off",
                                           "PrevPrice"
                                       }).skip(skip).limit(page)

        res = []
        for item in data:
            item['_id'] = str(item['_id'])
            item["Image"] = Tools.url_maker(item["Images"][0])
            if not category_list.__contains__(item["SubCategory"]):
                category_list.append(item["SubCategory"])
            res.append(item)

        same_res = []

        same_category = product_collection.aggregate([
            {"$match": {"SubCategory": {"$in": category_list}}},
            {"$sample": {"size": 10}},
            {"$skip": skip},
            {"$limit": page},
            {"$project": {"Images": 1,
                          "Category": 1,
                          "SubCategory": 1,
                          "UniqueValue": 1,
                          "Name": 1,
                          "Manufacture": 1,
                          "Off": 1,
                          "PrevPrice": 1}}])
        for item in same_category:
            item['_id'] = str(item['_id'])
            item["Image"] = Tools.url_maker(item["Images"][0])
            same_res.append(item)

        response = {
            "SearchResult": res,
            "SameProducts": same_res
        }
        return response

    @staticmethod
    def admin_search_products(key, page=1):
        skip, page = Tools.pagination(page)
        category_list = []
        data = product_collection.find({"$or": [{"Category": {'$regex': f'^{key}'}},
                                                {"SubCategory": {'$regex': f'^{key}'}},

                                                {"Name": {'$regex': f'^{key}'}},
                                                {"UniqueValue": {'$regex': f'^{key}'}},
                                                {"Manufacture": {'$regex': f'^{key}'}}]}, {
                                           "Images",
                                           "Category",
                                           "SubCategory",
                                           "UniqueValue",
                                           "Name",
                                           "Manufacture",
                                           "Off",
                                           "PrevPrice"
                                       }).skip(skip).limit(page)

        res = []
        for item in data:
            item["Image"] = Tools.url_maker(item["Images"][0])
            if not category_list.__contains__(item["Category"]):
                category_list.append(item["Category"])

            res.append(res)

        result = {
            "SearchedItem": res,
            "CategoriesInSearch": category_list
        }

        # return result
        # response = {
        #     "SearchResult": res,
        #     "SameProducts": same_res
        # }
        # print(response)

        return result
        # return 'd'

    @staticmethod
    def add_search_history(user_id, product_name, category, product_id, sub_category):
        result = search_history_collection.update_one({"UserId": user_id,
                                                       "ProductName": product_name,
                                                       "Category": category,
                                                       "SubCategory": sub_category},

                                                      {"$inc": {
                                                          "SearchCount": 1
                                                      }})
        if result.matched_count == 0:
            search_history_collection.insert_one(SearchHistoryModel(**{"UserId": user_id,
                                                                       "ProductName": product_name,
                                                                       "Category": category,
                                                                       "SubCategory": sub_category}).dict())
        return Tools.Result(True, 'd')

    @staticmethod
    def __get_last_search_hostory(user_id, number=10):
        result = search_history_collection.find({"UserId": user_id}).limit(number).sort([("_id", pymongo.DESCENDING)])
        res = []
        for item in result:
            item["_id"] = str(item["_id"])
            res.append(item)
        return res

    @staticmethod
    def recommendation_depend_of_history(user_id, page=1):
        same_res, searches = Search.search_recommendation(page, user_id)

        # print(same_res)

        return SearchPageRecommendationModel(Searches=searches, Recommendation=same_res)

    @staticmethod
    def search_recommendation(page, user_id):
        searches = Search.__get_last_search_hostory(user_id, 10)
        skip, page = Tools.pagination(page)
        sub_category = list(set([item["SubCategory"] for item in searches]))
        same_category = product_collection.aggregate([
            {"$match": {"SubCategory": {"$in": sub_category}}},
            {"$sample": {"size": 10}},
            {"$skip": skip},
            {"$limit": page},
            {"$project": {"Images": 1,
                          "Category": 1,
                          "UniqueValue": 1,
                          "Name": 1,
                          "Manufacture": 1,
                          "Off": 1,
                          "PrevPrice": 1}}])
        same_res = Tools.make_result_ready_product(same_category, 'Images')
        return same_res, searches

    @staticmethod
    def admin_homepage_destination_search(destination_name, key):
        valid_destination = ['Product', 'Category']
        if destination_name not in valid_destination:
            raise HTTPException(detail=f'destination should be {valid_destination}', status_code=400)
        res = []
        if destination_name == 'Product':
            items = product_collection.find({"Name": {'$regex': f'^{key}'}}, {"_id", "Name"})
            for item in items:
                item["_id"] = str(item['_id'])
                res.append(item)
        elif destination_name == 'Category':
            items = category_collection.find({"name": {'$regex': f'^{key}'}}, {"_id", "name"})
            for item in items:
                item["_id"] = str(items['_id'])
                res.append(item)
        return {
            "Destination": destination_name,
            "Data": res
        }
