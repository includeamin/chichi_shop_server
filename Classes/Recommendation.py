from Database.DB import product_collection
from Classes.Tools import Tools


class Recommendation:
    @staticmethod
    def get_recommendation_of_product_page(user_id=None, product_sub_category=None, page=1):
        same_res = []
        skip, page = Tools.pagination(page=page)
        same_category = product_collection.aggregate([
            {"$match": {"SubCategory": product_sub_category}},
            {"$sample": {"size": 20}},
            {"$skip": skip},
            {"$limit": page},
            {"$project": {"Images": 1,
                          "Category": 1,
                          "UniqueValue": 1,
                          "Name": 1,
                          "Manufacture": 1,
                          "Off": 1,
                          "PrevPrice": 1
                          }}])
        for item in same_category:
            item['_id'] = str(item['_id'])
            item["Image"] = Tools.url_maker(item["Images"][0])
            same_res.append(item)

        return same_res

