from Classes.Product import Product
from Classes.Tools import Tools
from Classes.Checkout import Checkout
from Database.DB import basket_collection
from ResponseModel.Basket.basket_response import BasketResponse
from bson import ObjectId
import re
import math
from Classes.ShopConfig import ShopConfig
from Classes.Tools import Tools
from Classes import Bridge
from fastapi import HTTPException
from datetime import datetime


class Basket:
    ware_house_projection = {
        "code", "time"
    }

    valid_payment_types = ["online", "cash"]

    Basket_states = {
        "Current": "Current",
        "UserInformationDeliveryConfirm": "UserInformationDeliveryConfirm",
        "ConfirmPaymentInfo": "ConfirmPaymentInfo",
        "WaitForWareHouse": "WaitForWareHouse",
        "Gathering": "Gathering",
        "WaitForAssign": "WaitForAssign",
        "Assigned": "Assigned",
        "ReceivedFromWareHouse": "ReceivedFromWareHouse",
        "Checked": "Checking",
        "Delivering": "Delivering",
        "WaitForCustomer": "WaitForCustomer",
        "DeliveredToCustomer": "DeliveredToCustomer",
        "ReturnToWareHouse": "ReturnToWareHouse",
        "Survey": "Survey"

    }

    # TODO: index on : UserId
    # TODO: add support for canceling in different stages
    # TODO: add images in add_to_basket

    def __init__(self, user_id, product_id, prev_price, current_price, product_name, images, count):
        self.UserId = user_id
        self.Products = [{
            'ProductId': product_id,
            'Count': count,
            'PrevPrice': prev_price,
            'CurrentPrice': current_price,
            'Name': product_name,
            'Images': images
        }]
        # todo: check and delete
        self.BasketId = None
        self.UserInfo = {
            'UserId': user_id,
            'Name': None,
            'PhoneNumber': None,
            'Location': None,
            'Receipt': None
        }
        self.ChosenDelivery = {
            'Price': None,
            'Type': None
        }
        self.Payment = {
            'Type': None,
            'IsPaid': False
        }

        self.PackageInfo = {
            'PackageNumber': None,
            'EstimatedTime': None,
            'State': None
        }

        self.BasketState = Basket.Basket_states["Current"]
        self.StateChangingTiming = {
            "Current": datetime.now().timestamp(),
            "UserInformationDeliveryConfirm": None,
            "ConfirmPaymentInfo": None,
            "WaitForWareHouse": None,
            "Gathering": None,
            "WaitForAssign": None,
            "Assigned": None,
            "ReceivedFromWareHouse": None,
            "Checked": None,
            "Delivering": None,
            "WaitForCustomer": None,
            "DeliveredToCustomer": None,
            "ReturnToWareHouse": None,
            "Survey": None
        }

    # Checkout related

    class Constants:
        phone_regex = r'[0-9]{11}'

        delivery_types = ['Motor', 'Car', 'Bike']

    @staticmethod
    def system_convert_none_current_baskets_that_not_processed(user_id):
        basket = basket_collection.find_one(
            {"UserInfo.UserId": user_id, '$or': [{"BasketState": Basket.Basket_states["UserInformationDeliveryConfirm"],
                                                  }, {
                                                     "BasketState": Basket.Basket_states["ConfirmPaymentInfo"]
                                                 }]}, {'_id'})
        if not basket:
            return False

        basket_collection.update_one({"_id": basket['_id']}, {"$set": {"BasketState": Basket.Basket_states["Current"]}})
        return True

    @staticmethod
    def system_session_check_basket_is_fill(user_id):
        basket = basket_collection.find_one({"UserInfo.UserId": user_id}, {"Products.Count"})
        return len(basket["Products"])

    @staticmethod
    def get_users_current_basket(user_id):
        data = basket_collection.find_one({"UserInfo.UserId": user_id, "BasketState": Basket.Basket_states["Current"]})
        return data

    @staticmethod
    def state_manager(current, next_state):
        if next_state not in Basket.Basket_states.keys():
            raise Exception("invalid state")
        next_state_index = list(Basket.Basket_states.keys()).index(next_state)
        current_state_index = list(Basket.Basket_states.keys()).index(current)
        # check some state that can ignore them
        if next_state_index - current_state_index > 1:
            raise Exception("state cant change")

    @staticmethod
    def get_next_state(current):
        if current not in Basket.Basket_states.keys():
            raise Exception("invalid state")
        next_state_index = list(Basket.Basket_states.keys()).index(current)
        if next_state_index == len(Basket.Basket_states.keys()) - 1:
            raise Exception("end of state list, cant move to next state")
        next_state_key = list(Basket.Basket_states.keys())[next_state_index + 1]
        print("-----state change log ----")
        print(next_state_key)
        print('---')
        return Basket.Basket_states[next_state_key]

    @staticmethod
    def get_current_state(basket_id):
        basket = basket_collection.find_one({"_id": ObjectId(basket_id)}, {"BasketState"})
        if not basket:
            raise HTTPException(detail="basket not found", status_code=400)
        return basket["BasketState"]

    @staticmethod
    def validate_address(address):
        if ((address is not None and ('City' in address) and ('Street' in address) and
             ('PostalCode' in address)) or address is None):
            return True
        else:
            return False

    @staticmethod
    def validate_geo(geo):
        if ((geo is not None and ('Lat' in geo) and ('Lang' in geo)
             and isinstance(geo['Lat'], float) and isinstance(geo['Lang'], float))
                or geo is None):
            return True
        else:
            return False

    @staticmethod
    def validate_location(location):
        if 'Name' not in location or not Basket.validate_address(location['Address']) or not Basket.validate_geo(
                location['Geo']):
            return False
        else:
            return True

    @staticmethod
    def validate_phone(phone):
        return re.match(Basket.Constants.phone_regex, phone)

    @staticmethod
    def validate_delivery_type(delivery_type):
        if delivery_type in Basket.Constants.delivery_types:
            return True
        else:
            return False

    @staticmethod
    def submit_user_info(user_id, name, phone_number, location, receipt, delivery_type):
        # users_basket = Basket.get_users_current_basket(user_id)
        users_basket = basket_collection.find_one(
            {"UserInfo.UserId": user_id, "BasketState": Basket.Basket_states["Current"]})
        if not users_basket:
            raise HTTPException(detail='user has not basket', status_code=400)

        user_geo = location['Geo']
        shop_center_geo = ShopConfig.get_shop_center_geo()
        # todo :    new function
        distance = math.sqrt(
            ((user_geo['Lat'] - shop_center_geo['Lat']) ** 2) +
            ((user_geo['Lang'] - shop_center_geo['Lang']) ** 2)
        )

        cost = ShopConfig.get_distance_cost(distance)

        package_number = ShopConfig.next_package_number()

        set_dict = Checkout(
            str(users_basket["_id"]),
            user_id,
            name,
            phone_number,
            location,
            receipt,
            package_number,
            delivery_type,
            cost[delivery_type]).__dict__
        # add new baskets state for changing state
        set_dict["BasketState"] = Basket.get_next_state(users_basket["BasketState"])
        # set change time of state
        # set_dict["StateChangingTiming"][Basket.get_next_state(users_basket["BasketState"])] = datetime.now()

        # insert user info and submit it
        _id = basket_collection.update_one({
            "_id": users_basket["_id"]
        },
            {
                '$set': {
                    **set_dict,
                    f"StateChangingTiming.{Basket.get_next_state(users_basket['BasketState'])}": datetime.now().timestamp()
                }
            })

        return Tools.Result(True, {'PackageCode': '{:08d}'.format(package_number),
                                   'BasketId': str(users_basket["_id"])})

    @staticmethod
    def delete_checkout_data(basket_id):
        # todo : reset checkout info
        # basket_collection.update_one({'BasketId': basket_id},{""})

        basket_collection.delete_one({'BasketId': basket_id})

    @staticmethod
    def get_price_for_delivery(latitude, longitude):

        shop_center_geo = ShopConfig.get_shop_center_geo()

        distance = math.sqrt(
            ((float(latitude) - shop_center_geo['Lat']) ** 2) +
            ((float(longitude) - shop_center_geo['Lang']) ** 2)
        )

        cost = ShopConfig.get_distance_cost(distance)

        return Tools.Result(True, Tools.dumps(cost))

    @staticmethod
    def submit_payment_info(user_id, payment_type):
        if payment_type not in Basket.valid_payment_types:
            raise HTTPException(detail=f'payment should be {Basket.valid_payment_types}'
                                , status_code=400)
        # basket = Basket.get_users_current_basket(user_id)
        basket = basket_collection.find_one(
            {"UserInfo.UserId": user_id, "BasketState": Basket.Basket_states["UserInformationDeliveryConfirm"]})
        if not basket:
            raise HTTPException(detail="basket not found", status_code=400)
        #
        # if basket["BasketState"] != "UserInformationDeliveryConfirm":
        #     raise HTTPException(detail="user information not confirmed", status_code=400)
        # todo : check price of basket for checking availability of payment in cash or not
        current_datetime = datetime.now().timestamp()
        # check if payment type is cash, move to WaitForWareHouse state instead of ConfirmPaymentInfo state
        # and left StateChangingTiming of WaitForPayment, None
        # cash: UserInformationDeliveryConfirm --> WaitForWareHouse
        # online: UserInformationDeliveryConfirm --> ConfirmPaymentInfo --> WaitForWareHouse
        basket_state = "WaitForWareHouse" if payment_type == 'cash' else Basket.get_next_state(basket["BasketState"])
        # update payment info
        result = basket_collection.update_one(
            {
                # 'UserInfo.UserId': user_id,
                "_id": basket['_id']
                # 'BasketId': basket_id
            },
            {'$set': {
                'Payment.Type': payment_type,
                'BasketState': basket_state,
                f"StateChangingTiming.{basket_state}": current_datetime
            }}
        )
        if result.modified_count == 0:
            raise HTTPException(detail="no baskets updated", status_code=400)

        return Tools.Result(True, 'd')

    @staticmethod
    def finalize_payment(user_id):

        # user_basket = Basket.get_users_current_basket(user_id)
        user_basket = basket_collection.find_one({"UserInfo.UserId": user_id,
                                                  "BasketState": Basket.Basket_states["ConfirmPaymentInfo"]})
        if not user_basket:
            raise HTTPException(detail="basket not found", status_code=400)

        # TODO: store data about payment

        # make basket checked out
        result = basket_collection.update_one(
            {
                '_id': user_basket["_id"]
            },
            {'$set': {'BasketState': Basket.get_next_state(user_basket["BasketState"]),
                      f"StateChangingTiming.{Basket.get_next_state(user_basket['BasketState'])}": datetime.now().timestamp()
                      }}
        )
        if result.modified_count == 0:
            raise HTTPException(detail='nothing changed', status_code=400)
        return 'd'

    # Basket related
    @staticmethod
    def off_calculator(off: dict, prev_price: int):
        return prev_price * (1 - off["Percentage"])

    @staticmethod
    def add_to_basket(user_id, product_id, number):

        # make sure product id is valid
        product = Product.is_product_id_valid_id(product_id)

        if product is None:
            return Tools.Result(False, Tools.errors('INF'))

        user_has_basket = basket_collection.find_one({
            # '_id': ObjectId(basket_id),
            'UserId': user_id,
            'BasketState': "Current"
            # 'IsCurrent': True
        })

        if user_has_basket:
            # try to update the count
            update_result = basket_collection.update_one(
                {'_id': user_has_basket["_id"], 'BasketState': 'Current'},
                {'$inc': {'Products.$[elem].Count': number}},
                array_filters=[{'elem.ProductId': product_id}]
            )

            # if update did not affect then it is the first time we are inserting this product
            if update_result.modified_count == 0:
                basket_collection.update_one(
                    {'_id': user_has_basket["_id"], 'BasketState': 'Current'},
                    {
                        '$push': {
                            'Products': {
                                'ProductId': product_id,
                                'Count': number,
                                'PrevPrice': product['PrevPrice'],
                                'CurrentPrice': Basket.off_calculator(product["Off"], product["PrevPrice"]),
                                'Name': product['Name'],
                                'Images': product['Images'][0]
                            }
                        }
                    }
                )
        else:

            user_has_basket = basket_collection.find_one({
                'UserId': user_id,
                'BasketState': 'Current'
            }) is not None

            if user_has_basket:
                return Tools.Result(False, Tools.errors('NA'))

            # create a new basket for user
            _id = basket_collection.insert_one(Basket(
                user_id,
                product_id,
                product['PrevPrice'],
                Basket.off_calculator(product["Off"], product["PrevPrice"]),
                product['Name'],
                product['Images'][0],
                number
            ).__dict__)

            return Tools.Result(True, str(_id.inserted_id))

        return Tools.Result(True, 'd')

    @staticmethod
    def get_basket_id(user_id):
        basket_object = basket_collection.find_one({
            'UserInfo.UserId': user_id
        },
            {'_id': 1, "BasketState": 1})
        if basket_object is None:
            return Tools.Result(False, Tools.errors('INF'))

        response = {
            'Id': str(basket_object['_id']),
            'State': basket_object["BasketState"]
        }

        return Tools.Result(True, response)

    @staticmethod
    def delete_from_basket(user_id, product_id):
        # make sure product id is valid
        is_valid = Product.is_product_id_valid_id(product_id) is not None

        if not is_valid:
            return Tools.Result(False, Tools.errors('INF'))

        users_basket = basket_collection.find_one(
            {"UserInfo.UserId": user_id, 'BasketState': Basket.Basket_states["Current"]}, {'Products': 1})

        if not users_basket:
            raise HTTPException(detail="basket not found", status_code=404)

        # make sure user has a basket containing the product with specified product id
        # products_object = basket_collection.find_one({
        #     '_id': ObjectId(basket_id),
        #     'UserInfo.UserId': user_id,
        #     'Products.ProductId': product_id
        # },
        #     {'Products': 1}
        # )
        #
        # if products_object is None:
        #     return Tools.Result(False, Tools.errors('INF'))

        # get count of the product
        count = 0
        for product in users_basket['Products']:
            if product['ProductId'] == product_id:
                count = product['Count']

        if count == 1:
            # delete the product from products array
            basket_collection.update_one(
                {
                    '_id': users_basket["_id"],
                    'UserId': user_id
                },
                {'$pull': {'Products': {'ProductId': product_id}}})
        else:
            # decrement the count
            basket_collection.update_one(
                {
                    '_id': users_basket["_id"],
                    'Products.ProductId': product_id
                },
                {'$inc': {'Products.$.Count': -1}}
            )

        return Tools.Result(True, 'd')

    @staticmethod
    def delete_all_instances(user_id, product_id):
        # make sure product id is valid
        is_valid = Product.is_product_id_valid_id(product_id) is not None

        if not is_valid:
            return Tools.Result(False, Tools.errors('INF'))

        users_basket = basket_collection.find_one(
            {"UserInfo.UserId": user_id, 'BasketState': Basket.Basket_states["Current"]}, {'Products': 1})

        if not users_basket:
            raise HTTPException(detail="basket not found", status_code=404)
        #
        # # make sure user has a basket containing the product with specified product id
        # products_object = basket_collection.find_one({
        #     '_id': ObjectId(basket_id),
        #     'UserInfo.UserId': user_id,
        #     'Products.ProductId': product_id
        # },
        #     {'Products': 1}
        # )
        #
        # if products_object is None:
        #     return Tools.Result(False, Tools.errors('INF'))

        # delete the product from products array
        basket_collection.update_one(
            {
                '_id': users_basket["_id"],
                'UserId': user_id
            },
            {'$pull': {'Products': {'ProductId': product_id}}})

        return Tools.Result(True, 'd')

    @staticmethod
    def get_basket_items(user_id):

        user_basket = Basket.get_users_current_basket(user_id)

        if not user_basket:
            raise HTTPException(detail="user has not basket", status_code=400)

        # make sure user has an open basket
        basket_object = basket_collection.find_one({
            '_id': user_basket["_id"],

        })

        if basket_object is None:
            raise HTTPException(detail='user has not basket', status_code=400)
        # todo : uncomment
        # remove checkout data about basket
        # Basket.delete_checkout_data(basket_id)

        products = []
        total_correntprice = 0.0
        total_discount = 0.0
        products_object = basket_object['Products']
        for product in products_object:
            product["Images"] = Tools.url_maker(product["Images"])
            products.append(product)
            total_correntprice += product["CurrentPrice"] * product["Count"]
            total_discount += (product["PrevPrice"] - product["CurrentPrice"]) * product["Count"]

        return Tools.Result(True,
                            BasketResponse(user_id, products, total_correntprice, total_discount).__dict__)

    @staticmethod
    def get_basket_info(package_code: int):

        basket_object = basket_collection.find_one(
            {
                'PackageInfo.PackageNumber': package_code,
                # 'UserId': user_id
            },
            {'Products': 1})

        if basket_object is None:
            return None, None, None

        products = basket_object['Products']

        basket_price = 0
        total_discount = 0

        for product in products:
            basket_price += product["CurrentPrice"] * product["Count"]
            total_discount += (product["PrevPrice"] - product["CurrentPrice"]) * product["Count"]

        return basket_price, total_discount, products

    @staticmethod
    def get_summery_of_payment(package_code: int):
        basket_object = basket_collection.find_one(
            {
                'PackageInfo.PackageNumber': package_code
            },
            {'Products': 1, "ChosenDelivery": 1})

        if basket_object is None:
            raise HTTPException(detail="not found", status_code=404)
        products = basket_object['Products']

        basket_price = 0
        total_discount = 0
        for product in products:
            basket_price += product["CurrentPrice"] * product["Count"]
            total_discount += (product["PrevPrice"] - product["CurrentPrice"]) * product["Count"]

        total_amount_of_payment = basket_price + basket_object["ChosenDelivery"]["Price"]

        return {
            "TotalToPay": total_amount_of_payment,
            "TotalBasketAmount": basket_price,
            "TotalDiscount": total_discount,
            "DeliveryPrice": basket_object["ChosenDelivery"]["Price"]
        }
