from Database.DB import shop_config_collection
from Classes.Tools import Tools


class ShopConfig:

    def __init__(self):
        self.PackageCode = 0
        self.ProductGatheringMessage = 'پکیج با موفقیت جمع آوری شد'
        self.OperatorReceivedPackage = 'اپراتور پکیج شما رو دریافت کرد'
        self.UniqueValue = 'Name'
        self.CenterShopGeo = {
            'Lat': 15.9,
            'Lang': 10.3
        }
        self.ValidCategories = [
            'گروه ۲',
            'گروه ۱',
        ]
        self.DistanceCost = [
            {'Min': 0, 'Max': 5, 'Cost': {
                'Motor': 1,
                'Car': 2,
                'Bike': 3
            }},
            {'Min': 6, 'Max': 10, 'Cost': {
                'Motor': 6,
                'Car': 7,
                'Bike': 8
            }},
            {'Min': 11, 'Max': 15, 'Cost': {
                'Motor': 11,
                'Car': 12,
                'Bike': 13
            }},
            {'Min': 16, 'Max': 20, 'Cost': {
                'Motor': 16,
                'Car': 17,
                'Bike': 18
            }},
            {'Min': 21, 'Max': 25, 'Cost': {
                'Motor': 19,
                'Car': 20,
                'Bike': 21
            }},
            {'Min': 26, 'Max': 'INF', 'Cost': {
                'Motor': 30,
                'Car': 31,
                'Bike': 32
            }}
        ]

    @staticmethod
    def init_config():
        shop_config_collection.insert_one(ShopConfig().__dict__)
        return Tools.Result(True, 'd')

    @staticmethod
    def next_package_number():
        config_object = shop_config_collection.find_one({}, {'PackageCode': 1})

        package_code = config_object['PackageCode']

        # increment package code
        shop_config_collection.update_one({}, {'$inc': {'PackageCode': 1}})

        return package_code

    @staticmethod
    def get_gather_product_message():
        config_object = shop_config_collection.find_one({}, {'ProductGatheringMessage': 1})

        gathering_message = config_object['ProductGatheringMessage']

        return gathering_message

    @staticmethod
    def get_operator_received_package_message():
        config_object = shop_config_collection.find_one({}, {'OperatorReceivedPackage': 1})

        operator_receive_message = config_object['OperatorReceivedPackage']

        return operator_receive_message

    @staticmethod
    def set_distance_cost(distances):

        for distance in distances:
            if 'Max' not in distance or 'Min' not in distance:
                return Tools.Result(False, Tools.errors('NA'))

        shop_config_collection.update_one({}, {
            '$push': {*distances}
        })

        return Tools.Result(True, 'd')

    @staticmethod
    def get_all_distance_cost():

        config_object = shop_config_collection.find_one({}, {'DistanceCost': 1})

        distance_cost = config_object['DistanceCost']

        return Tools.Result(True, Tools.dumps(distance_cost))

    @staticmethod
    def get_valid_categories():

        config_object = shop_config_collection.find_one({}, {'ValidCategories': 1})

        valid_categories = config_object['ValidCategories']

        return Tools.Result(True, Tools.dumps(valid_categories))

    @staticmethod
    def inner_get_valid_categories():

        config_object = shop_config_collection.find_one({}, {'ValidCategories': 1})

        valid_categories = config_object['ValidCategories']

        return valid_categories

    @staticmethod
    def get_distance_cost(distance_val):

        distances = shop_config_collection.find_one({}, {'_id': 0, 'DistanceCost': 1})

        for distance in distances['DistanceCost']:
            if distance['Max'] == 'INF' or (distance['Min'] <= distance_val <= distance['Max']):
                return distance['Cost']

        return None

    @staticmethod
    def set_product_gathering_message(message):
        shop_config_collection.update_one({}, {
            '$set': {'ProductGatheringMessage': message}
        })

        return Tools.Result(True, 'd')

    @staticmethod
    def set_operator_received_package_message(message):
        shop_config_collection.update_one({}, {
            '$set': {'OperatorReceivedPackage': message}
        })

        return Tools.Result(True, 'd')

    @staticmethod
    def set_valid_categories(valid_categories):
        shop_config_collection.update_one({}, {
            '$set': {'ValidCategories': valid_categories}
        })

        return Tools.Result(True, 'd')

    @staticmethod
    def set_shop_center_geo(geo):
        if 'Lang' not in geo or 'Lat' not in geo:
            return Tools.Result(False, Tools.errors('NA'))

        shop_config_collection.update_one({}, {
            '$set': {'CenterShopGeo.Lat': geo['Lat'], 'CenterShopGeo.Lang': geo['Lang']}
        })

        return Tools.Result(True, 'd')

    @staticmethod
    def get_shop_center_geo():
        shop_config_object = shop_config_collection.find_one({}, {'_id': 0, 'CenterShopGeo': 1})

        return shop_config_object['CenterShopGeo']

    @staticmethod
    def get_unique_value():
        unique_value_config = shop_config_collection.find_one({}, {'UniqueValue': 1, '_id': 0})

        return unique_value_config

    @staticmethod
    def set_unique_value(unique_value):
        shop_config_collection.find_one({}, {
            '$set': {'UniqueValue': unique_value}
        })

        return Tools.Result(True, 'd')
