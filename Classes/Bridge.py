import requests
from Classes.Tools import Tools
import json
import logging
from fastapi import HTTPException

Result = Tools.Result
Dumps = Tools.dumps
Error = Tools.errors


def send_authentication_email(email, code):
    try:
        # get dynamicurl
        content = requests.post("https://chichiapp.ir:3008/email/authentication/send",
                                data={"Email": email, "Code": code}, verify=False).content
        logging.warning(content)

        # content = json.loads(content)
        return content

    except Exception as ex:
        return Result(False, ex.args)


def gen_token_authentication(user_id):
    try:

        content = requests.post('{0}/system/users/token/add'.format('http://chichiapp.ir:30031'),
                                data={"UserId": user_id}, verify=False).content
        logging.warning(content)
        content = json.loads(content)

        if content["State"]:
            # log
            return content["Description"]
        else:
            return False
    except Exception as ex:
        logging.warning(ex.args)
        return False


def is_auth(user_id, token):
    try:
        content = requests.get('{0}/users/auth/check/{1}/{2}'.format("http://chichiapp.ir:30031",
                                                                     user_id,
                                                                     token), verify=False).content
        content = json.loads(content)

        if content["State"]:
            return True
        else:
            return False
    except Exception as ex:
        logging.warning(ex.args)
        return False


def log_out(user_id, token):
    try:
        content = requests.get(
            '{0}system/users/logoout/{1}/{2}'.format('http://chichiapp.ir:30031/', user_id, token),
            verify=False).content
        content = json.loads(content)
        if content['State']:
            return True
        else:
            return False
    except Exception as ex:
        return Result(False, ex.args)


def send_sms(phone_number, message):
    return {
        'State': True
    }


def send_basket_data_to_chichi_man(data):
    content = requests.post('{}system/chichiman/assign'.format('http://chichiapp.ir:30033/'), json=data)
    print(content.content)
    if content.status_code != 200:
        raise HTTPException(detail=str(content.content), status_code=400)


    # content = json.loads(content)

    # print(content)
    # if content['State']:
    #     return True
    # else:
    #     return False


def get_coins_box_items():
    return {"Box1": 100}


def get_exps_dic():
    return {"Box1": 100}


def get_point_dics():
    return {"Box1": 100}


def get_golden_coin_dics():
    return {"Box1": 100}

# if __name__ == '__main__':
# a = is_auth('5cdc39eeb321f68b2b9e6174','acbca7bcc5da74b4208cd51263bed1e7cf475e6a2e45962cd35f4affbe703f35')
# print(a)
