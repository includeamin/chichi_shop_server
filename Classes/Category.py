from Database.DB import category_collection, product_collection
from datetime import datetime
from fastapi import HTTPException
from Classes.Tools import Tools
from bson import ObjectId


class Category:
    def __init__(self, name, image):
        self.name = name
        self.create_at = datetime.now()
        self.update_at = datetime.now()
        self.image = image
        self.sub_categories = []
        self.items_count = 0
        self.View = 0

    @staticmethod
    def admin_add(name, image):
        package = category_collection.find_one({"name": name})
        if package:
            raise HTTPException(detail='same package already exist', status_code=400)
        item = category_collection.insert_one(Category(name=name, image=image).__dict__)
        return str(item.inserted_id)

    @staticmethod
    def admin_update_image(_id, image):
        package = category_collection.find_one({"_id": ObjectId(_id)})
        if not package:
            raise HTTPException(detail='same package already exist', status_code=400)
        result = category_collection.update_one({"_id": package["_id"]}, {'$set': {'image': image}})
        if result.modified_count == 0:
            return 'nothing updated'
        return 'd'

    @staticmethod
    def admin_add_sub_category(category_name, sub_category_name):
        result = category_collection.update_one({"name": category_name}, {'$addToSet': {
            "sub_categories": sub_category_name
        }})

        if result.matched_count == 0:
            raise HTTPException(detail='nothing changed, category with this name not found', status_code=400)
        else:
            return 'd'

    @staticmethod
    def admin_remove_sub_category(category_name, sub_category_name):
        result = category_collection.update_one({"name": category_name}, {'$pull': {
            "sub_categories": sub_category_name
        }})

        if result.matched_count == 0:
            raise HTTPException(detail='nothing changed, category with this name not found', status_code=400)
        else:
            return 'd'

    @staticmethod
    def system_add_item_to_category(name):
        category_collection.update_one({"name": name}, {"$inc": {
            "item_count": 1
        }})

    @staticmethod
    def system_remove_item_from_category(name):
        category_collection.update_one({"name": name}, {"$inc": {
            "item_count": -1
        }})

    @staticmethod
    def admin_get_all():
        res = []
        items = category_collection.find({})
        for item in items:
            item['_id'] = str(item['_id'])
            item['image'] = Tools.url_maker(item['image'])
            res.append(item)

        return res

    @staticmethod
    def delete(name):
        category_collection.delete_one({'name': name})
        return 'd'

    @staticmethod
    def admin_get_by(key, value):
        if key == 'name':
            item = category_collection.find_one({'name': value})
            item['_id'] = str(item['_id'])
            item['image'] = Tools.url_maker(item['image'])
            return item
        elif key == 'id':
            item = category_collection.find_one({"_id": ObjectId(value)})
            item['_id'] = str(item['_id'])
            item['image'] = Tools.url_maker(item['image'])
            return item
        else:
            return 'wrong key'

    @staticmethod
    def system_get_category_name_by_id(_id):
        category = category_collection.find_one({"_id":ObjectId(_id)},{"name"})
        return category["name"] if category else None

    @staticmethod
    def get_all_category_name():
        items = category_collection.find({}, {"sub_categories", 'name', 'image'})
        res = []
        for item in items:
            item["_id"] = str(item['_id'])
            item['image'] = Tools.url_maker(item['image'])
            res.append(item)
        return {"Result": res}

    @staticmethod
    def get_category_for_sub_category_mobile_page():
        pipline = [
            {"$sample": {"size": 4}},
            {"$project": {"name": 1, "image": 1}}
        ]
        query = category_collection.aggregate(pipeline=pipline)
        res = []
        for item in query:
            item["image"] = Tools.url_maker(item['image'])
            item["_id"] = str(item['_id'])
            res.append(item)
        return res

    @staticmethod
    def add_click_count():
        # todo : impl click count
        pass

    @staticmethod
    def mobile_category_detail():
        pass

    @staticmethod
    def homepage_get_category_images_list():
        res = []
        categories = category_collection.find({}, {"image", "_id", 'name'})
        for item in categories:
            item["_id"] = str(item["_id"])
            res.append(item)
        return res
