from bson import ObjectId
from datetime import datetime
import requests
import json

from Classes import Search
from Classes.Tools import Tools
from Classes.ShopConfig import ShopConfig
from Database.DB import product_collection
from ResponseModel.Product.product_response import ProductResponse
from Classes.Recommendation import Recommendation
from fastapi import HTTPException
from Model.Products import ProductDetailPage
from Classes.Category import Category
from Classes.Search import Search


class Product:

    # TODO index on unique value
    # TODO write function to sync prices in database and basket
    # TODO query database to find best seller and most viewed
    def __init__(self, UniqueValue, Name=None, Attribute=None, Manufacture=None, Count: int = None, Create_at=None,
                 Updated_at=None, price: int = None, CurrentPrice: int = None, Description=None, Category=None,
                 Images: list = None, SalesCount=None, is_off_enabled=False, off=0.0, sub_category=None):
        self.UniqueValue = UniqueValue
        self.Name = Name
        self.Count = Count
        self.SalesCount = SalesCount
        self.ViewCount = 0
        self.Create_at = Create_at if Create_at is not None else datetime.now()
        self.Updated_at = Updated_at if Updated_at is not None else datetime.now()
        self.Attribute = Attribute
        self.Manufacture = Manufacture
        self.PrevPrice = price
        self.CurrentPrice = CurrentPrice
        self.Description = Description
        self.Off = {
            "Enable": is_off_enabled,
            "Percentage": off
        }
        self.Category = Category
        self.SubCategory = sub_category
        self.Images = [*Images] if Images is not None else []

    @staticmethod
    def add_product(unique_value, name, attribute, manufacture, count, prev_price: int, created_at, updated_at,
                    description, category, images: list, off_percentage: float,
                    is_off_enabled: bool, sub_category):

        # make sure product is unique
        is_duplicated = product_collection.find_one({
            'UniqueValue': unique_value
        }) is not None

        if is_duplicated:
            return Tools.Result(False, Tools.errors('IAE'))
        # todo : check valid category with category db
        # valid_categories = ShopConfig.inner_get_valid_categories()
        #
        # if category not in valid_categories:
        #     return Tools.Result(False, Tools.errors('NA'))

        # insert into product collection
        item = product_collection.insert_one(Product(
            unique_value,
            name,
            attribute,
            manufacture,
            count,
            created_at if created_at is not None else datetime.now(),
            updated_at if updated_at is not None else datetime.now(),
            prev_price,
            0,
            description,
            category,
            images, off=off_percentage, is_off_enabled=is_off_enabled, sub_category=sub_category
        ).__dict__)

        return Tools.Result(True, str(item.inserted_id))

    @staticmethod
    def update_product(unique_value, name, attribute, manufacture, count, prev_price: int,
                       description, category, images: list, off_percentage: float,
                       is_off_enabled: bool, sub_category: str, _id: str):

        # make sure product id is valid
        is_valid = product_collection.find_one({'_id': ObjectId(_id)})

        if not is_valid:
            return Tools.Result(False, Tools.errors('INF'))

        if str(is_valid["UniqueValue"]).strip() != str(unique_value).strip():
            # make sure combination of (name, attribute, manufacture) is unique
            is_duplicated = product_collection.find_one({
                'Name': name,
                'Attribute': attribute,
                'Manufacture': manufacture,
                'UniqueValue': {'$not': {'$eq': unique_value}}
            }) is not None

            if is_duplicated:
                return Tools.Result(False, Tools.errors('IAE'))

        # todo : validate category

        # valid_categories = ShopConfig.inner_get_valid_categories()
        #
        # if category not in valid_categories:
        #     return Tools.Result(False, Tools.errors('NA'))

        # update product info
        product_collection.update_one(
            {'_id': ObjectId(_id)},
            {
                '$set': {
                    'Name': name,
                    'Attribute': attribute,
                    'Manufacture': manufacture,
                    'Count': count,
                    'PrevPrice': prev_price,
                    'CurrentPrice': 0,
                    'Description': description,
                    'Category': category,
                    'Images': images,
                    'Update_at': datetime.now(),
                    'Off': {
                        "Enable": is_off_enabled,
                        "Percentage": off_percentage
                    },
                    "SubCategory": sub_category
                }
            })

        return Tools.Result(True, 'd')

    @staticmethod
    def delete_product(unique_value):

        # make sure product id is valid
        is_valid = product_collection.find_one({'UniqueValue': unique_value}) is not None

        if not is_valid:
            return Tools.Result(False, Tools.errors('INF'))

        # delete the product
        product_collection.delete_one({'UniqueValue': unique_value})

        return Tools.Result(True, 'd')

    @staticmethod
    def get_products(page=1):
        current = page

        products = []
        skip, page = Tools.pagination(page=page)

        # return products
        products_cursor = product_collection.find({}).skip(skip).limit(page)

        for product in products_cursor:
            product["_id"] = str(product["_id"])
            if product["Off"]["Enable"]:
                product["CurrentPrice"] = product["PrevPrice"] * (1 - product["Off"]["Percentage"])
            image_list = []
            for item in product["Images"]:
                image_list.append(Tools.url_maker(item))

            product["Images"] = image_list

            products.append(product)

        return Tools.Result(True, {
            "Products": products,
            "Page": current
        })

    @staticmethod
    def get_product(_id):

        # make sure product id is valid
        is_valid = product_collection.find_one({'_id': ObjectId(_id)}) is not None

        if not is_valid:
            return Tools.Result(False, Tools.errors('INF'))

        product = product_collection.find_one({'_id': ObjectId(_id)})

        product["_id"] = str(product["_id"])
        image_list = []
        for item in product["Images"]:
            image_list.append(Tools.url_maker(item))

        product["Images"] = image_list

        return Tools.Result(True,
                            product
                            )

    @staticmethod
    def admin_get_product(_id):
        item = product_collection.find_one({"_id": ObjectId(_id)})
        if not item:
            raise HTTPException(detail="item not found", status_code=400)
        item["_id"] = str(item["_id"])
        images = []
        for image in item["Images"]:
            images.append(Tools.url_maker(image))

        item.pop('Images')
        item["Images"] = images

        return item

    @staticmethod
    def is_product_id_valid(unique_value: str):
        unique_value = unique_value.strip()
        item = product_collection.find_one({'UniqueValue': unique_value})
        print("value", unique_value)

        print('item', item)
        return product_collection.find_one({'UniqueValue': unique_value})

    @staticmethod
    def is_product_id_valid_id(id):
        if not ObjectId.is_valid(id):
            raise HTTPException(detail="Invalid Product Id", status_code=400)

        return product_collection.find_one({'_id': ObjectId(id)})

    @staticmethod
    def get_product_by_category(sub_category, page=1):

        # valid_categories = ShopConfig.inner_get_valid_categories()

        # if category not in valid_categories:
        #     return Tools.Result(False, Tools.errors('NA'))
        # todo : add check categories validate
        current = page

        products = []
        skip, page = Tools.pagination(page=page)

        # return products
        products_cursor = product_collection.find({"SubCategory": sub_category}).skip(skip).limit(page)

        for product in products_cursor:
            product["_id"] = str(product["_id"])
            product["CurrentPrice"] = Product.off_calculator(product["Off"], product["PrevPrice"])
            image_list = []
            for item in product["Images"]:
                image_list.append(Tools.url_maker(item))

            product["Images"] = image_list

            products.append(product)

        return Tools.Result(True, {
            "Products": products,
            "Page": current
        })

    @staticmethod
    def __sync_products(products):

        for product in products:
            unique_value = product['UniqueValue']
            product.pop('UniqueValue')

            # if product is found update it
            product_object = product_collection.find_one({'UniqueValue': unique_value}, {'CurrentPrice': 1})

            if product_object is not None:
                current_price = product_object['CurrentPrice']

                if current_price != product['CurrentPrice']:
                    product['price'] = current_price

                product_collection.update_one(
                    {'UniqueValue': unique_value},
                    {
                        '$set': {
                            **product
                        }
                    }
                )
            # insert the new product
            else:
                product['UniqueValue'] = unique_value
                product_collection.insert_one(Product(**product).__dict__)

    @staticmethod
    def sync_all_products():
        # todo : should update

        unique_value = ShopConfig.get_unique_value()['UniqueValue']

        # get all products from server3008
        response = requests.get('http://chichiapp.ir:3050/Product/get')

        json_response = json.loads(response.content)

        products = json_response['Description']

        # make the data type uniform
        uniform_products = []
        for product in products:
            dict_ = {}

            if unique_value in product:
                dict_['UniqueValue'] = product[unique_value]
            else:
                raise Exception('{} must be present'.format(unique_value))

            dict_['Name'] = product['Name'] if 'Name' in product else None

            dict_['Count'] = product['Count'] if 'Name' in product else None

            dict_['SalesCount'] = product['SalesCount'] if 'SalesCount' in product else None

            dict_['ViewCount'] = product['ViewCount'] if 'ViewCount' in product else None

            dict_['Create_at'] = product['Create_at'] if 'Create_at' in product else None

            dict_['Update_at'] = product['Update_at'] if 'Update_at' in product else None

            dict_['Attribute'] = product['Attribute'] if 'Attribute' in product else None

            dict_['Manufacture'] = product['Manufacture'] if 'Manufacture' in product else None

            dict_['CurrentPrice'] = product['Cost'] if 'Cost' in product else None

            dict_['Description'] = product['Description'] if 'Description' in product else None

            dict_['Category'] = product['Category'] if 'Category' in product else None

            dict_['Images'] = product['Images'] if 'Images' in product else None

            uniform_products.append(dict_)

        Product.__sync_products(uniform_products)

        return Tools.Result(True, 'd')

    # todo : test
    @staticmethod
    def system_homepage_query(item, key, page=1):
        try:
            item_list = ["item-list", "category"]
            if not item_list.__contains__(item):
                raise Exception(Tools.errors("IVK"))
            if item == "item-list":
                return Product.test_item_list_home(page)

        except Exception as ex:
            return Tools.Result(False, ex.args)

    @staticmethod
    def test_item_list_home(page=1):
        skip, page = Tools.pagination(page)
        items = product_collection.find({}, {"Name", "Off", "Images", "Off", "PrevPrice", "CurrentPrice"}).skip(
            skip).limit(page
                        )
        res = []
        for item in items:
            item["Images"] = Tools.url_maker(item['Images'][0])
            item["CurrentPrice"] = Product.off_calculator(item["Off"], item["PrevPrice"])
            res.append(item)
        return Tools.Result(True, Tools.dumps(res))

    @staticmethod
    def add_off_to_products(product_id, percentage):
        pass

    @staticmethod
    def off_calculator(off, price):
        value = 0
        if off['Enable']:
            value = int(price) - int(price) * float(off["Percentage"])
        return value

    @staticmethod
    def mobile_product_page(_id):
        product = product_collection.find_one({"_id": ObjectId(_id)}, {
            "UniqueValue",
            "Name",
            "Manufacture",
            "PrevPrice",
            "Off",
            "Category",
            "Images",
            "SubCategory",
            "Description"
        })

        product["Image"] = Tools.url_maker(product["Images"][0])
        product.pop('Images')
        product["_id"] = str(product["_id"])

        if not product:
            raise HTTPException(detail='item not found', status_code=400)

        same = Recommendation.get_recommendation_of_product_page(product_sub_category=product['SubCategory'])

        return ProductDetailPage(Product=product, Recommendation=same)

    @staticmethod
    def test():
        # http://chichiapp.ir:3005/download/5d9884457c1e36d6e452598e
        product_collection.update_many({}, {"$set": {"Images": ["5d9884457c1e36d6e452598e"]}})
        return "Don"

    @staticmethod
    def load_products_of_sub_category(category_name=None, _id=None, page=1):
        if category_name is not None:
            match = {"$match": {"Category": category_name}}
        elif _id is not None:
            match = {"$match": {"Category": Category.system_get_category_name_by_id(_id)}}
        else:
            raise HTTPException(status_code=400, detail='category and _id are none')

        skip, page = Tools.pagination(page)
        pipline = [
            # {"$match": {"Category": category_name}},
            match,
            {"$limit": page}
            ,
            {"$skip": skip},
            {
                "$group": {
                    "_id": "$SubCategory",
                    "items": {"$addToSet": {
                        "_id": "$_id",
                        "Name": '$Name',
                        "PrevPrice": "$PrevPrice",
                        "Off": "$Off",
                        "CurrentPrice": {"$multiply": ['$PrevPrice', {"$subtract": [1, "$Off.Percentage"]}]},
                        "Image": {"$arrayElemAt": ["$Images", 0]}
                    }}
                },

            }
        ]

        data = product_collection.aggregate(pipeline=pipline)
        res = []
        for item in data:
            for i in item["items"]:
                i["_id"] = str(i["_id"])
                i["Image"] = Tools.url_maker(i["Image"])
            item["SubCategory"] = item['_id']
            item.pop('_id')
            res.append(item)

        sample_of_category = Category.get_category_for_sub_category_mobile_page()
        # res.append({"OtherCategories": sample_of_category})

        response = {
            "Data": res,
            "OtherCategories": sample_of_category
        }

        return Tools.status_code_result('200', response)

    @staticmethod
    def load_more_sub_category(sub_category_name):
        pass
        # skip, page = Tools.pagination(page)
        # pipline = [
        #     # {"$match": {"Category": category_name}},
        #     match,
        #     {"$limit": page}
        #     ,
        #     {"$skip": skip},
        #     {
        #         "$group": {
        #             "_id": "$SubCategory",
        #             "items": {"$addToSet": {
        #                 "_id": "$_id",
        #                 "Name": '$Name',
        #                 "PrevPrice": "$PrevPrice",
        #                 "Off": "$Off",
        #                 "CurrentPrice": {"$multiply": ['$PrevPrice', {"$subtract": [1, "$Off.Percentage"]}]},
        #                 "Image": {"$arrayElemAt": ["$Images", 0]}
        #             }}
        #         },
        #
        #     }
        # ]

    @staticmethod
    def load_more(user_id, key, value=None, page=1):
        valid_load_more_keys = [
            "sub-category",
            "item-list",
            "product-recommendation",
            "search-recommendation"
        ]
        if key not in valid_load_more_keys:
            raise HTTPException(detail=f'key should be in {valid_load_more_keys}', status_code=404)
        if key == 'item-list':
            # here value is QueryKey
            return json.loads(Product.system_homepage_query(key, value, page=page)["Description"])
        if key == 'sub-category':
            # here value is CategoryName
            return Recommendation.get_recommendation_of_product_page(product_sub_category=value)
        if key == 'product-recommendation':
            # value here is subcategory
            return Recommendation.get_recommendation_of_product_page(product_sub_category=value)
        if key == 'search-recommendation':
            # value here is list of subcategories
            return Search.recommendation_depend_of_history(user_id, page=page)
