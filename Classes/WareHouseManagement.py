from Classes.Product import Product
from Classes.Tools import Tools
from Classes.Checkout import Checkout
from Database.DB import basket_collection
from Classes.Basket import Basket
from ResponseModel.Basket.basket_response import BasketResponse
from bson import ObjectId
import re
import math
from Classes.ShopConfig import ShopConfig
from Classes.Tools import Tools
from Classes import Bridge
from fastapi import HTTPException
from datetime import datetime


class WareHouse:
    ware_house_valid_state = {
        "WaitForWareHouse": Basket.Basket_states["WaitForWareHouse"],
        "Gathering": Basket.Basket_states["Gathering"],
        "WaitForAssign": Basket.Basket_states["WaitForAssign"]
    }

    @staticmethod
    def calculate_gathering_timing():
        return 204

    @staticmethod
    def ware_house_get_basket_depends_of_state(state=ware_house_valid_state["WaitForWareHouse"]):
        data = basket_collection.find({'BasketState': state}, {
            "PackageInfo.PackageNumber",
            f"StateChangingTiming.{state}"
        })
        res = []
        for item in data:
            item["_id"] = str(item["_id"])
            item["TimeToGathering"] = WareHouse.calculate_gathering_timing()
            res.append(item)
        return res

    @staticmethod
    def get_valid_ware_house_state():
        return list(WareHouse.ware_house_valid_state.keys())

    @staticmethod
    def ware_house_get_package_info(basket_id):
        package = basket_collection.find_one({"_id": ObjectId(basket_id)},
                                             {"Products", "_id", "UserInfo.Receipt",
                                              "StateChangingTiming.Gathering"
                                                 , "StateChangingTiming.WaitForWareHouse"
                                                   "BasketState"})
        total_price = 0
        package["_id"] = str(package["_id"])
        if not package:
            raise HTTPException(detail="basket not exist", status_code=400)
        for item in package["Products"]:
            item["Images"] = Tools.url_maker(item["Images"])
            total_price += item["CurrentPrice"]

        package["TotalPrice"] = total_price
        return package

    @staticmethod
    def confirm_package_received(basket_id):

        # make sure basket is checked out
        # if Basket.get_basket_state(user_id, basket_id) != 'IsCheckedOut':
        #     return Tools.Result(False, Tools.errors('NA'))

        # users_basket = basket_collection.find_one({"_id": ObjectId(basket_id)})
        print(Basket.Basket_states['WaitForWareHouse'])
        users_basket = basket_collection.find_one(
            {"_id": ObjectId(basket_id), "BasketState": Basket.Basket_states['WaitForWareHouse']})
        print(users_basket)
        if not users_basket:
            raise HTTPException(detail="basket not found", status_code=404)

        # make sure payment is confirmed
        # user_object = basket_collection.find_one({
        #     "_id": users_basket['_id'], 'StateChangingTiming.ConfirmPaymentInfo': {"$ne": None}
        # }, {'_id': 1, 'UserInfo': 1})
        if users_basket["Payment"]["Type"] == 'online':
            if not users_basket["StateChangingTiming"]["ConfirmPaymentInfo"]:
                raise HTTPException(detail="payment not confirmed", status_code=400)

        basket_collection.update_one(
            {'_id': users_basket["_id"]},
            {'$set': {
                'BasketState': Basket.get_next_state(users_basket["BasketState"]),
                f'StateChangingTiming.{Basket.get_next_state(users_basket["BasketState"])}': datetime.now().timestamp()
            }}
        )

        # # set basket status to being processed
        # Basket.set_basket_status_to_being_processed(user_id, basket_id)

        Bridge.send_sms(users_basket['UserInfo']['PhoneNumber'],
                        ShopConfig.get_operator_received_package_message())
        return 'd'

    @staticmethod
    def confirm_package_gathering(basket_id):
        # make sure basket is received by the operator
        # if Basket.get_basket_state(user_id, basket_id) != 'BeingProcessed':
        #     print(Basket.get_basket_state(user_id, basket_id))
        #     return Tools.Result(False, Tools.errors('NA'))
        users_basket = basket_collection.find_one(
            {'_id': ObjectId(basket_id), "BasketState": Basket.Basket_states["Gathering"]})
        # if Basket.get_current_state(users_basket["_id"]) != "Gathering":
        #     raise HTTPException(detail="wrong state", status_code=400)

        if users_basket is None:
            raise HTTPException(detail="basket not found", status_code=404)

        basket_collection.update_one(
            {'_id': users_basket["_id"]},  # add _id to query keys
            {'$set': {
                'BasketState': Basket.get_next_state(users_basket["BasketState"]),
                f'StateChangingTiming.{Basket.get_next_state(users_basket["BasketState"])}': datetime.now().timestamp()
            }}
        )

        Bridge.send_sms(users_basket['UserInfo']['PhoneNumber'],
                        ShopConfig.get_gather_product_message())
        users_basket = basket_collection.find_one({'_id': ObjectId(basket_id)})

        # check response
        total_basket_price, _, products = Basket.get_basket_info(
            users_basket['PackageInfo']['PackageNumber'])
        data = {
            'Products': products,
            'UserId': users_basket["UserInfo"]["UserId"],
            'Name': users_basket['UserInfo']['Name'],
            'PhoneNumber': users_basket['UserInfo']['PhoneNumber'],
            'Location': users_basket['UserInfo']['Location'],
            'Receipt': users_basket['UserInfo']['Receipt'],
            'PackageNumber': users_basket['PackageInfo']['PackageNumber'],
            'TotalBasketPrice': total_basket_price,
            'DeliveryPrice': users_basket['ChosenDelivery']['Price'],
            "BasketState": users_basket["BasketState"],
            "StateChangingTiming": users_basket["StateChangingTiming"]
        }

        Bridge.send_basket_data_to_chichi_man(data)
        basket_collection.delete_one({"_id": users_basket["_id"]})

        # set basket state to being gathered and assigned
        # Basket.set_basket_status_to_being_gathered_and_assigned(
        #     user_id, basket_id)
        return 'd'
