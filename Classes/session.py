from Classes.Basket import Basket


class SessionActions:
    @staticmethod
    def system_start(user_id):
        basket_current_maker = Basket.system_convert_none_current_baskets_that_not_processed(user_id=user_id)
        basket_len = Basket.system_session_check_basket_is_fill(user_id=user_id)

        return {
            "basket_current_maker": basket_current_maker,
            "basket_product_count": basket_len
        }
