import re
import math

# import Classes.Basket as Basket

from Classes.ShopConfig import ShopConfig
from Classes.Tools import Tools
from Classes import Bridge
from Database.DB import checkout_collection


class Checkout:

    def __init__(self, basket_id, user_id, name, phone_number, location, receipt, package_number, delivery_type,
                 delivery_price):
        self.BasketId = basket_id
        self.UserInfo = {
            'UserId': user_id,
            'Name': name,
            'PhoneNumber': phone_number,
            'Location': location,
            'Receipt': receipt
        }
        self.ChosenDelivery = {
            'Price': delivery_price,
            'Type': delivery_type
        }
        self.Payment = {
            'Type': None,
            'IsPaid': False
        }

        self.PackageInfo = {
            'PackageNumber': package_number,
            'EstimatedTime': None,
            'State': None
        }

        # self.IsUserInfoConfirmed = True
        # self.IsDeliveryTypeConfirmed = True
        # self.IsPaymentConfirmed = False
        #
        # self.OperatorReceivedPackage = False
        # self.PackageGathered = False
        # self.IsCheckedOut = False

    # class Constants:
    #     phone_regex = r'[0-9]{11}'

    #     delivery_types = ['Motor', 'Car', 'Bike']

    # @staticmethod
    # def validate_address(address):
    #     if ((address is not None and ('City' in address) and ('Street' in address) and
    #          ('PostalCode' in address)) or address is None):
    #         return True
    #     else:
    #         return False

    # @staticmethod
    # def validate_geo(geo):
    #     if ((geo is not None and ('Lat' in geo) and ('Lang' in geo)
    #          and isinstance(geo['Lat'], float) and isinstance(geo['Lang'], float))
    #             or geo is None):
    #         return True
    #     else:
    #         return False

    # @staticmethod
    # def validate_location(location):
    #     if 'Name' not in location or not Checkout.validate_address(location['Address']) or not Checkout.validate_geo(
    #             location['Geo']):
    #         return False
    #     else:
    #         return True

    # @staticmethod
    # def validate_phone(phone):
    #     return re.match(Checkout.Constants.phone_regex, phone)

    # @staticmethod
    # def validate_delivery_type(delivery_type):
    #     if delivery_type in Checkout.Constants.delivery_types:
    #         return True
    #     else:
    #         return False

    # @staticmethod
    # def submit_user_info(user_id, basket_id, name, phone_number, location, receipt, delivery_type):

    #     # validate location and phone number
    #     if not Checkout.validate_location(location) \
    #             or not Checkout.validate_phone(phone_number) \
    #             or not Checkout.validate_delivery_type(delivery_type):
    #         return Tools.Result(False, Tools.errors('NA'))

    #     # make sure user has a confirmed and not checked out basket
    #     has_basket = Basket.Basket.has_basket(user_id)

    #     if not has_basket:
    #         return Tools.Result(False, Tools.errors('INF'))

    #     user_geo = location['Geo']
    #     shop_center_geo = ShopConfig.get_shop_center_geo()

    #     distance = math.sqrt(
    #         ((user_geo['Lat'] - shop_center_geo['Lat']) ** 2) +
    #         ((user_geo['Lang'] - shop_center_geo['Lang']) ** 2)
    #     )

    #     cost = ShopConfig.get_distance_cost(distance)

    #     package_number = ShopConfig.next_package_number()

    #     # insert user info and submit it
    #     _id = checkout_collection.insert_one(Checkout(
    #         basket_id,
    #         user_id,
    #         name,
    #         phone_number,
    #         location,
    #         receipt,
    #         package_number,
    #         delivery_type,
    #         cost[delivery_type]
    #     ).__dict__)

    #     print(_id.inserted_id)

    #     # set basket status as being checked out
    #     Basket.Basket.set_basket_status_to_being_checked_out(user_id, basket_id)

    #     return Tools.Result(True, Tools.dumps({'PackageCode': '{:08d}'.format(package_number)}))

    # @staticmethod
    # def delete_checkout_data(basket_id):

    #     checkout_collection.delete_one({'BasketId': basket_id})

    # @staticmethod
    # def get_price_for_delivery(latitude, longitude):

    #     shop_center_geo = ShopConfig.get_shop_center_geo()

    #     distance = math.sqrt(
    #         ((float(latitude) - shop_center_geo['Lat']) ** 2) +
    #         ((float(longitude) - shop_center_geo['Lang']) ** 2)
    #     )

    #     cost = ShopConfig.get_distance_cost(distance)

    #     return Tools.Result(True, Tools.dumps(cost))

    # @staticmethod
    # def submit_chosen_delivery(user_id, basket_id, delivery_type):

    #     # validate delivery type
    #     if not Checkout.validate_delivery_type(delivery_type):
    #         print(delivery_type)
    #         return Tools.Result(False, Tools.errors('NA'))

    #     # make sure basket is in checking out state
    #     if Basket.Basket.get_basket_state(user_id, basket_id) != 'BeingCheckedOut':
    #         print(Basket.Basket.get_basket_state(user_id, basket_id))
    #         return Tools.Result(False, Tools.errors('NA'))

    #     # make sure user info is submitted before
    #     user_object = checkout_collection.find_one(
    #         {
    #             'UserInfo.UserId': user_id,
    #             'BasketId': basket_id,
    #             'IsUserInfoConfirmed': True,
    #             'IsCheckedOut': False
    #         },
    #         {'_id': 1, 'CalculatedPrice': 1})

    #     if user_object is None:
    #         return Tools.Result(False, Tools.errors('NA'))

    #     # submit delivery info
    #     checkout_collection.update_one(
    #         {
    #             'UserInfo.UserId': user_id,
    #             'BasketId': basket_id
    #         },
    #         {
    #             '$set': {
    #                 'ChosenDelivery': {
    #                     'Price': user_object['CalculatedPrice'][delivery_type],
    #                     'Type': delivery_type
    #                 },
    #                 'IsDeliveryTypeConfirmed': True
    #             }
    #         }
    #     )

    #     return Tools.Result(True, 'd')

    # @staticmethod
    # def submit_payment_info(user_id, basket_id, payment_type):

    #     # make sure basket is in checking out state
    #     if Basket.Basket.get_basket_state(user_id, basket_id) != 'BeingCheckedOut':
    #         return Tools.Result(False, Tools.errors('NA'))

    #     # make sure delivery type is confirmed
    #     is_delivery_type_confirmed = checkout_collection.find_one(
    #         {
    #             'UserInfo.UserId': user_id,
    #             'BasketId': basket_id,
    #             'IsDeliveryTypeConfirmed': True,
    #             'IsCheckedOut': False
    #         },
    #         {'_id': 1}
    #     ) is not None

    #     if not is_delivery_type_confirmed:
    #         return Tools.Result(False, Tools.errors('NA'))

    #     # update payment info
    #     checkout_collection.update_one(
    #         {
    #             'UserInfo.UserId': user_id,
    #             'BasketId': basket_id
    #         },
    #         {'$set': {
    #             'Payment.Type': payment_type,
    #             'IsPaymentConfirmed': True
    #         }}
    #     )

    #     return Tools.Result(True, 'd')

    # @staticmethod
    # def finalize_payment(user_id, basket_id):

    #     # make sure basket is in checking out state
    #     if Basket.Basket.get_basket_state(user_id, basket_id) != 'BeingCheckedOut':
    #         return Tools.Result(False, Tools.errors('NA'))

    #     # make sure payment type is confirmed
    #     is_payment_type_confirmed = checkout_collection.find_one(
    #         {
    #             'UserInfo.UserId': user_id,
    #             'BasketId': basket_id,
    #             'IsPaymentConfirmed': True,
    #             'IsCheckedOut': False
    #         },
    #         {'_id': 1}
    #     ) is not None

    #     if not is_payment_type_confirmed:
    #         return Tools.Result(False, Tools.errors('NA'))

    #     # TODO: store data about payment

    #     # make basket checked out
    #     checkout_collection.update_one(
    #         {
    #             'UserInfo.UserId': user_id,
    #             'IsCheckedOut': False
    #         },
    #         {'$set': {'IsCheckedOut': True}}
    #     )

    #     # set basket status to checked out
    #     Basket.Basket.set_basket_status_to_checked_out(user_id, basket_id)

    # @staticmethod
    # def get_package_info(user_id, basket_id):

    #     # make sure basket is in checking out state
    #     if Basket.Basket.get_basket_state(user_id, basket_id) != 'BeingCheckedOut':
    #         return Tools.Result(False, Tools.errors('NA'))

    #     # make sure delivery type is confirmed
    #     is_delivery_type_confirmed = checkout_collection.find_one(
    #         {
    #             'UserInfo.UserId': user_id,
    #             'IsDeliveryTypeConfirmed': True,
    #             'IsCheckedOut': False
    #         },
    #         {'_id': 1}
    #     ) is not None

    #     if not is_delivery_type_confirmed:
    #         return Tools.Result(False, Tools.errors('NA'))

    #     # get basket total price and discount
    #     total_basket_price, total_discount, _ = Basket.Basket.get_basket_info(user_id, basket_id)

    #     if total_basket_price is None:
    #         return Tools.Result(False, Tools.Result(False, Tools.errors('INF')))

    #     # get delivery price
    #     checkout_object = checkout_collection.find_one(
    #         {'UserInfo.UserId': user_id}, {'ChosenDelivery': 1})

    #     delivery_price = checkout_object['ChosenDelivery']['Price']

    #     total_price = delivery_price + total_basket_price

    #     response = {
    #         'BasketPrice': total_basket_price,
    #         'TotalDiscount': total_discount,
    #         'DeliveryPrice': delivery_price,
    #         'TotalPrice': total_price
    #     }

    #     return Tools.Result(True, Tools.dumps(response))

    # @staticmethod
    # def confirm_package_received(user_id, basket_id):

    #     # make sure basket is checked out
    #     if Basket.Basket.get_basket_state(user_id, basket_id) != 'IsCheckedOut':
    #         return Tools.Result(False, Tools.errors('NA'))

    #     # make sure payment is confirmed
    #     user_object = checkout_collection.find_one({
    #         'UserInfo.UserId': user_id,
    #         'BasketId': basket_id,
    #         'IsPaymentConfirmed': True
    #     }, {'_id': 1})

    #     if user_object is not None:
    #         return Tools.Result(False, Tools.errors('INF'))

    #     checkout_collection.update_one(
    #         {'UserInfo.UserId': user_id},
    #         {'$set': {'OperatorReceivedPackage': True}}
    #     )

    #     # set basket status to being processed
    #     Basket.Basket.set_basket_status_to_being_processed(user_id, basket_id)

    #     Bridge.send_sms(user_object['UserInfo']['PhoneNumber'],
    #                     ShopConfig.get_operator_received_package_message())

    #     return Tools.Result(True, 'd')

    # # TODO: check for duplicated states
    # @staticmethod
    # def confirm_package_gathering(user_id, basket_id):

    #     # make sure basket is received by the operator
    #     if Basket.Basket.get_basket_state(user_id, basket_id) != 'BeingProcessed':
    #         print(Basket.Basket.get_basket_state(user_id, basket_id))
    #         return Tools.Result(False, Tools.errors('NA'))

    #     user_object = checkout_collection.find_one(
    #         {
    #             'UserInfo.UserId': user_id,
    #             'OperatorReceivedPackage': True
    #         },
    #         {'_id': 1, 'UserInfo': 1, 'ChosenDelivery': 1, 'PackageInfo': 1}
    #     )

    #     if user_object is None:
    #         return Tools.Result(False, Tools.errors('INF'))

    #     checkout_collection.update_one(
    #         {'UserInfo.UserId': user_id},
    #         {'$set': {'IsPackageGathered': True}}
    #     )

    #     Bridge.send_sms(user_object['UserInfo']['PhoneNumber'],
    #                     ShopConfig.get_gather_product_message())

    #     total_basket_price, _, products = Basket.Basket.get_basket_info(
    #         user_id, basket_id)
    #     data = {
    #         'Products': products,
    #         'UserId': user_id,
    #         'Name': user_object['UserInfo']['Name'],
    #         'PhoneNumber': user_object['UserInfo']['PhoneNumber'],
    #         'Location': user_object['UserInfo']['Location'],
    #         'Receipt': user_object['UserInfo']['Receipt'],
    #         'PackageNumber': user_object['PackageInfo']['PackageNumber'],
    #         'TotalBasketPrice': total_basket_price,
    #         'DeliveryPrice': user_object['ChosenDelivery']['Price']
    #     }

    #     result = Bridge.send_basket_data_to_chichi_man(data)

    #     # set basket state to being gathered and assigned
    #     Basket.Basket.set_basket_status_to_being_gathered_and_assigned(user_id, basket_id)

    #     if result is True:
    #         return Tools.Result(True, 'd')
    #     else:
    #         return Tools.Result(False, Tools.errors('INF'))
