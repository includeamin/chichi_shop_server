from bson import objectid
from flask import Flask, jsonify, request
from API.Product_Routes import product_route
from API.Basket_Routes import basket_route
from API.Checkout_Routes import checkout_route
from API.Shop_Config_Routes import shop_route
from flask_cors import CORS
from Classes.Tools import Tools

app = Flask(__name__)

app.register_blueprint(product_route)
app.register_blueprint(basket_route)
app.register_blueprint(checkout_route)
app.register_blueprint(shop_route)

CORS(app)

@app.route('/response', methods=['POST'])
def test():
    return jsonify({'response': request.get_json()['test']})

@app.route("/test")
def what():
    return jsonify({"What": "Authentication",
                    "Author": "AminJamal",
                    "NickName": "Includeamin",
                    "Email": "aminjamal10@gmail.com",
                    "WebSite": "includeamin.com"})

#  "MicroServiceManagementURl" : "http://10.110.45.200:3000/",

if __name__ == '__main__':
    print("Stat")
    app.run(host='0.0.0.0', port=3004, debug=True)
