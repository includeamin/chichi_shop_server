from pydantic import BaseModel


class CheckoutSubmitUserInfo(BaseModel):
    Name: str
    PhoneNumber: str
    Location: dict
    Receipt: bool
    DeliveryType: str


class SubmitPaymentInfo(BaseModel):
    PaymentType: str
