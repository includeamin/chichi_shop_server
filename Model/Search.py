from pydantic import BaseModel
from datetime import datetime


class SearchResponseModel(BaseModel):
    SearchResult: list
    SameProducts: list


class SearchHistoryModel(BaseModel):
    UserId: str
    ProductName: str
    Create_at:  datetime = datetime.now()
    Category: str
    SearchCount: int = 1


class SearchPageRecommendationModel(BaseModel):
    Searches: list
    Recommendation: list
