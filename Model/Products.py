from pydantic import BaseModel


class AdminAddProductModel(BaseModel):
    UniqueValue: str
    Name: str
    Attribute: str
    Manufacture: str
    Count: str
    Price: int
    Description: str
    Category: str
    Images: list
    Off: float = 0.0
    IsOffEnable: bool = False
    SubCategory: str


class AdminUpdateProductModel(BaseModel):
    Id:str
    UniqueValue: str
    Name: str
    Attribute: str
    Manufacture: str
    Count: str
    Price: int
    Description: str
    Category: str
    SubCategory: str
    Images: list
    Off: float
    IsOffEnable: bool


class ProductDetailPage(BaseModel):
    Product: dict
    Recommendation: list
