from pydantic import BaseModel


class BasketAddModel(BaseModel):
    BasketId: str = None
    ProductId: str
    Number: int


class DeleteItemFromBasketModel(BaseModel):
    ProductId: str


class DeleteAllModel(BaseModel):
    BasketId: str
    ProductId: str
    TotalPrice: float
    TotalDiscount: float

class DeleteAllFromBasket(BaseModel):
    ProductId: str
