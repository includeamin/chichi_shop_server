from pydantic import BaseModel


class AddCategoryModel(BaseModel):
    name:str
    image:str