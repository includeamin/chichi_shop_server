import requests
from datetime import datetime


def init_shop_config():
    a = requests.post("http://0.0.0.0:3004/admin/config/init",
                      json={},
                      headers={
                          'Id': '5d87e194549ae0267b5268cc',
                          'Token': '6109bfa925d615dc888c94d1ba858bad960f3dcb95a69453bd6dd1ba8acc4c49'
                      },
                      verify=False
                      ).content
    print(a)


if __name__ == "__main__":
    init_shop_config()
