import requests
from datetime import datetime


def add_to_basket():
    a = requests.post("http://0.0.0.0:3004/basket/add",
                      json={
                          'ProductId': '1', 
                          'Number': 1
                      },
                      headers={
                          'Id': '5d87e194549ae0267b5268cc',
                          'Token': '6109bfa925d615dc888c94d1ba858bad960f3dcb95a69453bd6dd1ba8acc4c49'
                      },
                      verify=False
                      ).content
    print(a)

def add_to_current_basket():
    a = requests.post("http://0.0.0.0:3004/basket/add",
                      json={
                          'ProductId': '1',
                          'Number': 3,
                          'BasketId': '5d89349733a046929055258b'
                      },
                      headers={
                          'Id': '5d87e194549ae0267b5268cc',
                          'Token': '6109bfa925d615dc888c94d1ba858bad960f3dcb95a69453bd6dd1ba8acc4c49'
                      },
                      verify=False
                      ).content
    print(a)


def get_current_basket_id():
    a = requests.get("http://0.0.0.0:3004/basket/id",
                      headers={
                          'Id': '5d87e194549ae0267b5268cc',
                          'Token': '6109bfa925d615dc888c94d1ba858bad960f3dcb95a69453bd6dd1ba8acc4c49'
                      },
                      verify=False
                      ).content
    print(a)


def delete_one_from_basket():
    a = requests.post("http://0.0.0.0:3004/basket/delete",
                      json={
                          'ProductId': '1',
                          'BasketId': '5d89349733a046929055258b'
                      },
                      headers={
                          'Id': '5d87e194549ae0267b5268cc',
                          'Token': '6109bfa925d615dc888c94d1ba858bad960f3dcb95a69453bd6dd1ba8acc4c49'
                      },
                      verify=False
                      ).content
    print(a)

def delete_all_instances_from_basket():
        a = requests.post("http://0.0.0.0:3004/basket/delete/all",
                      json={
                          'ProductId': '1',
                          'BasketId': '5d89349733a046929055258b'
                      },
                      headers={
                          'Id': '5d87e194549ae0267b5268cc',
                          'Token': '6109bfa925d615dc888c94d1ba858bad960f3dcb95a69453bd6dd1ba8acc4c49'
                      },
                      verify=False
                      ).content
        print(a)


def get_basket_items():
        a = requests.get("http://0.0.0.0:3004/basket/5d89349733a046929055258b",
                          headers={
                              'Id': '5d87e194549ae0267b5268cc',
                              'Token': '6109bfa925d615dc888c94d1ba858bad960f3dcb95a69453bd6dd1ba8acc4c49'
                          },
                          verify=False
                          ).content
        print(a)


def get_basket_items_by_state():
        a = requests.get("http://0.0.0.0:3004/basket/filter/IsCurrent",
                         headers={
                             'Id': '5d87e194549ae0267b5268cc',
                             'Token': '6109bfa925d615dc888c94d1ba858bad960f3dcb95a69453bd6dd1ba8acc4c49'
                         },
                         verify=False
                         ).content
        print(a)


if __name__ == "__main__":
    get_basket_items_by_state()
