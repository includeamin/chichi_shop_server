import requests
from datetime import datetime


def submit_user_info():
    a = requests.post("http://0.0.0.0:3004/checkout/submit/userinfo",
                      json={
                          'BasketId': '5d89349733a046929055258b',
                          'Name': 'amin',
                          'PhoneNumber': '09101063006',
                          'Location': {
                              'Name': 'home',
                              'Geo': {
                                  'Lat': 1.2,
                                  'Lang': 2.3
                              },
                              'Address': {
                                  'City': 'city',
                                  'Street': 'street',
                                  'PostalCode': '1920'
                              }
                          },
                          'Receipt': True,
                          'DeliveryType': 'Motor'
                      },
                      headers={
                          'Id': '5d87e194549ae0267b5268cc',
                          'Token': '6109bfa925d615dc888c94d1ba858bad960f3dcb95a69453bd6dd1ba8acc4c49'
                      },
                      verify=False
                      ).content
    print(a)


def get_delivery_price():
    a = requests.get("http://0.0.0.0:3004/checkout/delivery/prices/1.2/2.3",
                     headers={
                         'Id': '5d87e194549ae0267b5268cc',
                         'Token': '6109bfa925d615dc888c94d1ba858bad960f3dcb95a69453bd6dd1ba8acc4c49'
                     },
                     verify=False
                     ).content
    print(a)


def submit_payment():
    a = requests.post("http://0.0.0.0:3004/checkout/submit/paymentinfo",
                      json={
                          'BasketId': '5d89349733a046929055258b',
                          'PaymentType': 'Online'
                      },
                      headers={
                          'Id': '5d87e194549ae0267b5268cc',
                          'Token': '6109bfa925d615dc888c94d1ba858bad960f3dcb95a69453bd6dd1ba8acc4c49'
                      },
                      verify=False
                      ).content
    print(a)


def get_package_info():
    a = requests.get("http://0.0.0.0:3004/checkout/packageinfo/5d87e194549ae0267b5268cc/5d89349733a046929055258b",
                     headers={
                         'Id': '5d87e194549ae0267b5268cc',
                         'Token': '6109bfa925d615dc888c94d1ba858bad960f3dcb95a69453bd6dd1ba8acc4c49'
                     },
                     verify=False
                     ).content
    print(a)


def confirm_package_received():
    a = requests.post("http://0.0.0.0:3004/admin/checkout/confirm/received",
                      json={
                          'BasketId': '5d89349733a046929055258b'
                      },
                      headers={
                          'Id': '5d87e194549ae0267b5268cc',
                          'Token': '6109bfa925d615dc888c94d1ba858bad960f3dcb95a69453bd6dd1ba8acc4c49'
                      },
                      verify=False
                      ).content
    print(a)


def confirm_package_gathered():
    a = requests.post("http://0.0.0.0:3004/admin/checkout/confirm/gathering",
                      json={
                          'BasketId': '5d89349733a046929055258b'
                      },
                      headers={
                          'Id': '5d87e194549ae0267b5268cc',
                          'Token': '6109bfa925d615dc888c94d1ba858bad960f3dcb95a69453bd6dd1ba8acc4c49'
                      },
                      verify=False
                      ).content
    print(a)

if __name__ == "__main__":
    confirm_package_gathered()
