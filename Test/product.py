import requests
from datetime import datetime

def add_product():
    a = requests.post("http://0.0.0.0:3004/admin/product/add",
                      json={
                          'UniqueValue': '1', 
                          'Name': 'name1', 
                          'Attribute': 'att1', 
                          'Manufacture': 'manufacture1', 
                          'Count': 10, 
                          'Created_at': str(datetime.now()), 
                          'Updated_at': str(datetime.now()),
                          'price': 1000,
                          'CurrentPrice': 1000,  
                          'Description': 'description1', 
                          'Category': 'گروه ۲',
                          'Images': ['image1']

                      },
                      headers={
                          'Id': '5d87e194549ae0267b5268cc',
                          'Token': '6109bfa925d615dc888c94d1ba858bad960f3dcb95a69453bd6dd1ba8acc4c49'
                      },
                      verify=False
                      ).content
    print(a)


def update_product():
    a = requests.post("http://0.0.0.0:3004/admin/product/update",
                      json={
                          'UniqueValue': '1',
                          'Name': 'name1new',
                          'Attribute': 'att1new',
                          'Manufacture': 'manufacture1new',
                          'Count': 20,
                          'price': 2000,
                          'CurrentPrice': 2500,
                          'Description': 'description1new',
                          'Category': 'گروه ۲',
                          'Images': ['image1']

                      },
                      headers={
                          'Id': '5d87e194549ae0267b5268cc',
                          'Token': '6109bfa925d615dc888c94d1ba858bad960f3dcb95a69453bd6dd1ba8acc4c49'
                      },
                      verify=False
                      ).content
    print(a)

def get_all_products():
    a = requests.get("http://0.0.0.0:3004/admin/product/all",
                      headers={
                          'Id': '5d87e194549ae0267b5268cc',
                          'Token': '6109bfa925d615dc888c94d1ba858bad960f3dcb95a69453bd6dd1ba8acc4c49'
                      },
                      verify=False
                      ).content
    print(a)


def get_single_product():
    a = requests.get("http://0.0.0.0:3004/admin/product/1",
                     headers={
                         'Id': '5d87e194549ae0267b5268cc',
                         'Token': '6109bfa925d615dc888c94d1ba858bad960f3dcb95a69453bd6dd1ba8acc4c49'
                     },
                     verify=False
                     ).content
    print(a)


def get_product_by_category():
    a = requests.get("http://0.0.0.0:3004/admin/product/in/گروه ۲",
                     headers={
                         'Id': '5d87e194549ae0267b5268cc',
                         'Token': '6109bfa925d615dc888c94d1ba858bad960f3dcb95a69453bd6dd1ba8acc4c49'
                     },
                     verify=False
                     ).content
    print(a)


if __name__ == "__main__":
    get_product_by_category()
