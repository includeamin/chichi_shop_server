class ProductResponse:

    def __init__(self, unique_value, name, attribute, manufacture, count: int, create_at, updated_at,
                 prev_price: int, current_price: int, description, category, images: list):
        self.UniqueValue = unique_value
        self.Name = name
        self.Count = count
        self.Create_at = create_at
        self.Updated_at = updated_at
        self.Attribute = attribute
        self.Manufacture = manufacture
        self.PrevPrice = prev_price
        self.CurrentPrice = current_price
        self.Description = description
        self.Category = category
        self.Images = [*images]
