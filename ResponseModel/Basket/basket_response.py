class BasketResponse:

    def __init__(self, user_id, products, tp, td):
        self.UserId = user_id
        self.Products = [*products]
        self.TotalPrice: float = tp
        self.TotalDiscount: float = td
