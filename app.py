from fastapi import FastAPI, HTTPException
from starlette.responses import HTMLResponse, JSONResponse
from starlette.middleware.cors import CORSMiddleware
from uvicorn import run
from API.BasketFastApi import basket_route
from API.CheckoutFastApi import checkout_route
from API.ProductFastApi import product_route
from API.SearchApi import search_route
from API.CategoryFastApi import category_route
from Database.DB import product_collection, basket_collection
from API.WareHouseFastApi import ware_house
from API.SessionFastApi import session_route
app = FastAPI()

app.include_router(basket_route)
app.include_router(checkout_route)
app.include_router(product_route)
app.include_router(search_route)
app.include_router(category_route)
app.include_router(ware_house)
app.include_router(session_route)

origins = [
    "http://localhost.tiangolo.com",
    "https://localhost.tiangolo.com",
    "http://localhost",
    "http://localhost:8080",
    "http://localhost:3000"
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.exception_handler(HTTPException)
async def http_exception(request, exc):
    return JSONResponse({"detail": exc.detail}, status_code=exc.status_code)


@app.get("/", tags=["Main"])
def index():
    # homepage_collection.insert_one(home_page)

    return {"Email": "aminjamaml10@gmail.com", "Name": "amin jamal", "Nickname": "includeamin"}


@app.get('/healthz')
def check_liveness():
    inv = product_collection.find_one({}, {})
    return 'd'


if __name__ == '__main__':
    run(app, host='0.0.0.0', port=3004, debug=True)
